/* hal libraries*/
#include "stm32f4xx_hal.h"
#include "sensors_i2c.h"
#include "stm_memory.h"
#include "adc1.h"
#include "i2c1.h"
#include "spi1.h"
#include "dma.h"
/* other libraries */
#include "main.h"
#include "irc.h"
#include "led.h"
#include "control.h"
#include "motor.h"
#include "communication.h"
#include "myvariable.h"
#include "distance.h"
#include "speed.h"
#include "filters.h"
#include "model.h"

#define LED
#define ACC_GYRO
#define CONTROL
#define MEASUREMENT
#define COMMUNICATION

uint32_t main_loop_counter = 0; /**< Counter used in main loop. Divides some operations.*/

inline static uint32_t APP_WAKEUP(uint32_t freq){
	return !(main_loop_counter % ((MAIN_LOOP_FREQUENCY)/freq));
}
/* Private variable declarations ---------------------------------------------*/

volatile unsigned int packetCounter = 0;

/* Private function prototypes -----------------------------------------------*/
int main(void) {
	HAL_Init();
	SysTick_Config(SystemCoreClock / MAIN_LOOP_FREQUENCY);

#ifdef LED
	led_init();
#endif

	led_blue_on();
	led_yellow_on();
	led_red_on();


#ifdef ACC_GYRO
	MX_DMA_Init();
	MX_I2C1_Init();
	while(sensors_i2c_init() != HAL_OK){
		// if initialization fails try again.
		MX_DMA_Init();
		MX_I2C1_Init();
	}
#endif

#ifdef COMMUNICATION
	memory_init();
	spi_init();
	spi_start();
	communication_init();
#endif

#ifdef CONTROL
	control_init(1.0f /REG_SPEED_FREQUENCY);
	pwm_init();
#endif

#ifdef MEASUREMENT
	distance_init();
	adc_init();
#endif

	// measures motor i offset
	motor_set_pwm(0.0f);
	HAL_Delay(10);
	float f = 0.0f;
	for(int i = 0; i < 10000; i++){
		while(!motor_is_i()){
			// waiting...
		}
		motor_i_raw_handler();
		f += stm_out.motor_i /10000.0f;
	}
	motor_set_i_offset(f);

//stm_in.ref_pwm = 1.0f;
//stm_in.ref_speed = 0.5f;
//stm_in.reg = 1;
//stm_in.speed_meas_type = 0;

	while (1) {
		/* ### CONTINUES PART ### */
#ifdef ACC_GYRO
		sensors_read_start();
#endif

		stm_out.v_sc = SC_VOLTAGE_CONST * adc_to_V(ADC_SAMPLE_U_SC());
		stm_out.v_rail = RAIL_VOLTAGE_CONST * adc_to_V(ADC_SAMPLE_U_RAIL());

		if(motor_is_i()){
			motor_i_raw_handler();
		}

		if(motor_is_bemf()){
			motor_bemf_raw_handler();
			stm_out.speed_bemf  = V_BEMF_CC * MOTOR_ANG_TO_TRANS(stm_out.motor_bemf / MOTOR_K);
		}


#ifdef USE_IRC
		if(IRC_is()){
			stm_out.speed_irc = IRC_speed_conv(stm_out.motor_bemf > 0.0f);
			/*/
			stm_out.speed_irc = IRC_speed_conv(stm_out.applied_pwm > 0.0f);
			*/
		}
#endif

		/* === SYNCHRONIZED PART, 1ms period === */
		static uint32_t last_tick = 0;
		uint32_t tick = HAL_GetTick();
		if( tick == last_tick){
			continue;
		}else{
			main_loop_counter += tick - last_tick;
			last_tick = tick;
		}

#ifdef LED
		if (APP_WAKEUP(LED_BLINK_FREQUENCY)) {
			led_blue_toggle();
		}
#endif

#ifdef MEASUREMENT
		if (APP_WAKEUP(SPEED_UPDATE_FREQUENCY)) {
			stm_out.speed = speed_update(stm_out.speed_irc, stm_out.speed_bemf);
		}
#endif

#ifdef CONTROL
		//if (stm_out.v_rail > 3.0f) {
			control_stop_handler();
			control_pwm_handler();

			if (APP_WAKEUP(REG_SPEED_FREQUENCY)) {
				control_speed_handler();
			}

//		}else{
//			motor_set_pwm(0.0f);
//		}
#endif
	}
}

void HAL_ADCEx_InjectedConvCpltCallback(ADC_HandleTypeDef* hadc) {
	if (hadc == &hadc1) {
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);
		motor_IRQ_handler();
		IRC_step_IT_handler(ADC_SAMPLE_IRC());
		distance_process(ADC_SAMPLE_G3(), ADC_SAMPLE_G1());
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void) {
	while (1) {
		led_red_toggle();
		HAL_Delay(1000);
	}
}

