#ifndef MODEL_H_
#define MODEL_H_

/*
 * Car's constants
 */
#define MOTOR_K 		0.005f 		// motor constant
#define MOTOR_R			8.0f		// motor resistance
#define CAR_M 			0.150f 		// car's mass [kg]
//#define CAR_M 			0.01f 		// car's mass [kg]
#define WHEEL_RADIUS	0.01f 		// radius of the wheel[m]
#define CAR_N			1.0f/3.0f 	// gear ratio from motor to wheel axle [-]

#define MOTOR_ANG_TO_TRANS(_angular_)	((_angular_)*(WHEEL_RADIUS)*(CAR_N))

/* Constants are in full form for easy view, compiler will reduce them */
#define R12 3.3E3f
#define R14 10E3f
#define R1	10E3f
#define R50 R1

#define RAIL_VOLT_DIV		(R12)/(R12+R14)
#define SC_VOLT_DIV			(R1)/(R1+R50)

#define RAIL_VOLTAGE_CONST	1.0f/(RAIL_VOLT_DIV)
#define SC_VOLTAGE_CONST	1.0f/(SC_VOLT_DIV)

#define MOTOR_CURRENT_R 	(0.2f+(MOTOR_R)+1.0f)



#endif /* MODEL_H_ */
