#ifndef MAIN_H_
#define MAIN_H_

#define BEMF_MEASUREMENT_ENABLE
#define USE_IRC

/*
 * Correction constants
 */
#define IRC_CC 	1.0f
#define I_CC 	1.0f
#define V_BEMF_CC 1.0f

#ifdef BEMF_MEASUREMENT_ENABLE
	#define BEMF_CC 1.0f
#else
	#define BEMF_CC 1.0f
#endif

#define BEMF_FILT
#define I_FILT

inline int IS_READY(volatile int* x){
	if( *x > 0 ) {
		int foo = *x;
		*x = 0;
		return foo;
	}
	return *x;
}

/*
 * The frequency of IRQ of PWM.
 * We use central aligned PWM, therefore the PWM frequency is half.
 */
#define PWM_IRQ_FREQUENCY	40000

/*
 * Frequency of main endless loop [Hz].
 * It should divide SystemCoreClock without residue.
 * It should be divisible by 1000 without residue.
 */
#define MAIN_LOOP_FREQUENCY 1000

/*
 * Frequency of motor I control loop [Hz].
 * It should divide MAIN_LOOP_FREQUENCY without residue.
 */
#define REG_I_FREQUENCY 1000

/*
 * Frequency of speed control loop [Hz].
 * It should divide MAIN_LOOP_FREQUENCY without residue.
 */
#define REG_SPEED_FREQUENCY 200

/*
 * Frequency of distance control loop [Hz].
 * It should divide MAIN_LOOP_FREQUENCY without residue.
 */
//#define REG_DISTANCE_FREQUENCY 20

#define LED_BLINK_FREQUENCY	6

#define SPEED_UPDATE_FREQUENCY 		500
//#define DISTANCE_UPDATE_FREQUENCY	1000

void Error_Handler(void);
void main_ticks_inc(void);

#endif /* MAIN_H_ */
