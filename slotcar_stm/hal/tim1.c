#include "tim1.h"

TIM_HandleTypeDef htim1;

void tim1_pwm_init(uint32_t driver_period, uint32_t complementary_pwm){
	/* Enable Timer for PWM configuration*/
	TIM_MasterConfigTypeDef sMasterConfig;
//	TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;
	TIM_OC_InitTypeDef sConfigOC;

	/* Initializing Timer 1*/
	htim1.Instance = TIM1;
	htim1.Init.Prescaler = 0;
	htim1.Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
	htim1.Init.Period = driver_period;
	htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim1.Init.RepetitionCounter = 0;
	HAL_TIM_PWM_Init(&htim1);

//		/* Enabling TRGO for ADC1 injected conversion*/
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

	/* Configuring output channels of Tim1*/
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = (uint16_t) (((uint32_t)25 * (driver_period - 1)) / 100);
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCNPolarity = TIM_OCNPOLARITY_LOW;
	sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
	sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;

	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3);
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2);
	if ( complementary_pwm ){
		sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
		sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	}
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4);

	TIM_BreakDeadTimeConfigTypeDef c;
	c.DeadTime = 0xFF;
	HAL_TIMEx_ConfigBreakDeadTime(&htim1, &c);


	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);

	/*
	 * No IT from TIM1 itself.
	 */
//	HAL_TIMEx_PWMN_Start_IT(&htim1,TIM_CHANNEL_2);
//	HAL_TIMEx_PWMN_Start_IT(&htim1,TIM_CHANNEL_3);
//	HAL_TIMEx_PWMN_Start_IT(&htim1,TIM_CHANNEL_4);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim)
{
	UNUSED(htim);
	GPIO_InitTypeDef GPIO_InitStruct;
	/* Clock Inicialization*/
	__TIM1_CLK_ENABLE();
	__GPIOA_CLK_ENABLE();
	/* GPIO Inicialization*/
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	/* PA8 - ENA, PA9 - INA, PA10 - ENB, PA11 - INB */
	GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef *htim)
{
	UNUSED(htim);
//	__TIM1_CLK_DISABLE();
	HAL_GPIO_DeInit(GPIOA, GPIO_PIN_8 |GPIO_PIN_9| GPIO_PIN_10 | GPIO_PIN_11);
}
