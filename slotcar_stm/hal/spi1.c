#include "stm32f4xx.h"
#include "spi1.h"

/* SPI handler declaration */
SPI_HandleTypeDef hspi1;

int spi_init(void){
	  /*##-1- Configure the SPI peripheral #######################################*/
	  /* Set the SPI parameters */
	  hspi1.Instance               = SPI2;
	  //SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
	  hspi1.Init.Direction         = SPI_DIRECTION_2LINES;
	  hspi1.Init.CLKPhase          = SPI_PHASE_1EDGE;
	  hspi1.Init.CLKPolarity       = SPI_POLARITY_LOW;
	  hspi1.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
	  //SpiHandle.Init.CRCPolynomial     = 7;
	  hspi1.Init.DataSize          = SPI_DATASIZE_8BIT;
	  hspi1.Init.FirstBit          = SPI_FIRSTBIT_MSB;
	  hspi1.Init.NSS               = SPI_NSS_SOFT;
	  hspi1.Init.TIMode            = SPI_TIMODE_DISABLE;
	  hspi1.Init.Mode = SPI_MODE_SLAVE;

	  if(HAL_SPI_Init(&hspi1) != HAL_OK) {
		  return -1;
	  }else{
		  hspi1.Instance->CR1 &= ~SPI_CR1_SSI;
		  return 0;
	  }

}

void spi_start(){
	/* Enable TXE, RXNE and ERR interrupt */
	__HAL_SPI_ENABLE_IT(&hspi1,  SPI_IT_RXNE );
	/* Process Unlocked */
	__HAL_UNLOCK(&hspi1);
	/* Enable SPI peripheral */
	__HAL_SPI_ENABLE(&hspi1);
	hspi1.Instance->DR = 0xEE;
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
	UNUSED(hspi);
  GPIO_InitTypeDef  GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  /* Enable SPI clock */
  __HAL_RCC_SPI2_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* SPI SCK GPIO pin configuration  */
  GPIO_InitStruct.Pin       = GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin       = GPIO_PIN_12;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  //GPIO_InitStruct.Mode      = GPIO_MODE_IT_FALLING;
  //GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
  //GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  //HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Mode      = GPIO_MODE_IT_RISING_FALLING;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for SPI #########################################*/
  /* NVIC for SPI */
  HAL_NVIC_SetPriority(SPI2_IRQn, 1, 1);
  HAL_NVIC_EnableIRQ(SPI2_IRQn);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);


}

/**
  * @brief SPI MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO and NVIC configuration to their default state
  * @param hspi: SPI handle pointer
  * @retval None
  */
void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi)
{
	UNUSED(hspi);
  /*##-1- Reset peripherals ##################################################*/
	__HAL_RCC_SPI2_FORCE_RESET();
	__HAL_RCC_SPI2_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks ################################*/
  //HAL_GPIO_DeInit(GPIOB, GPIO_PIN_12 | GPIO_PIN_13| GPIO_PIN_14| GPIO_PIN_15);
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_13| GPIO_PIN_14| GPIO_PIN_15);

  /*##-3- Disable the NVIC for SPI ###########################################*/
  HAL_NVIC_DisableIRQ(SPI2_IRQn);
}





