/*
 * ADC settings.
 * Setting up regular and injected conversions.
 */

#ifndef ADC1_H_
#define ADC1_H_

#include "stm32f4xx.h"
#include "f4common.h"

// USER SETTING, please proceed carefully.

/*
 * @note ADC1_IN{X} is channel_{X}
 * VPROPI	- PC5 - ADC1_IN15
 * EMF_REF  - PC4 - ADC1_IN14 OK
 * Current 	- PC3 - ADC1_IN13
 * SC_U		- PC2 - ADC1_IN12 OK
 * DIF_U	- PC1 - ADC1_IN11 OK
 * U		- PC0 - ADC1_IN10 OK
 * G0		- PA0 - ADC1_IN0
 * G1		- PA1 - ADC1_IN1
 * G2		- PA2 - ADC1_IN2
 * G3		- PA3 - ADC1_IN3
 * G4		- PA5 - ADC1_IN5 OK
 * */


#define U_BEMF_CH		ADC_CHANNEL_11
#define U_BEMF_REF_CH	ADC_CHANNEL_14
#define U_SC_CH			ADC_CHANNEL_12
#define	U_RAIL_CH		ADC_CHANNEL_10
#define I_MOTOR_CH		ADC_CHANNEL_13
#define	I_DRIVER_CH		ADC_CHANNEL_15

//#define G0_CH			ADC_CHANNEL_0 // used as output
#define G1_CH			ADC_CHANNEL_1
//#define G2_CH			ADC_CHANNEL_2 // used as output
#define G3_CH			ADC_CHANNEL_3
// @note channel 4 is skipped.
#define G4_CH			ADC_CHANNEL_5

// regular group up to 16 conversions
// Ranks for regular conversion
/*!< The rank in the regular group sequencer.
 This parameter must be a number between Min_Data = 1 and Max_Data = 16 */
//#define ADC_IN0_RANK
#define ADC_IN1_RANK	1
//#define ADC_IN2_RANK
#define ADC_IN3_RANK	2
//#define ADC_IN4_RANK
#define ADC_IN5_RANK	3
//#define ADC_IN6_RANK
//#define ADC_IN7_RANK
//#define ADC_IN8_RANK
//#define ADC_IN9_RANK
#define ADC_IN10_RANK	4
//#define ADC_IN11_RANK
#define ADC_IN12_RANK	5
//#define ADC_IN13_RANK
//#define ADC_IN14_RANK
#define ADC_IN15_RANK	6

// Ranks for injected conversion
// injected group up to 4 conversions
#define ADC_INJ_CHANNELS	U_BEMF_REF_CH,\
							U_BEMF_CH,\
							I_MOTOR_CH,\
							I_DRIVER_CH

#define adc_get_inj1() (uint16_t)hadc1.Instance->JDR1
#define adc_get_inj2() (uint16_t)hadc1.Instance->JDR2
#define adc_get_inj3() (uint16_t)hadc1.Instance->JDR3
#define adc_get_inj4() (uint16_t)hadc1.Instance->JDR4

/*
 * ADC outputs
 */

#define ADC_SAMPLE_IRC ADC_SAMPLE_G4
//extern volatile uint16_t adcbuf;

void adc_init(void);

/* END OF USER SETTINGS */

#define adc_to_V(__voltage__) ((3.3f/4096.0f)*(float)(__voltage__))

extern ADC_HandleTypeDef hadc1;

typedef struct {
	uint32_t channel;
	uint32_t rank;
} pair_channel_rank;

static pair_channel_rank adc_regular_channels_ranks[] = {
#ifdef ADC_IN0_RANK
		{	ADC_CHANNEL_0, ADC_IN0_RANK},
#endif
#ifdef ADC_IN1_RANK
		{ ADC_CHANNEL_1, ADC_IN1_RANK },
#endif
#ifdef ADC_IN2_RANK
		{	ADC_CHANNEL_2, ADC_IN2_RANK},
#endif
#ifdef ADC_IN3_RANK
		{ ADC_CHANNEL_3, ADC_IN3_RANK },
#endif
#ifdef ADC_IN4_RANK
		{	ADC_CHANNEL_4, ADC_IN4_RANK},
#endif
#ifdef ADC_IN5_RANK
		{ ADC_CHANNEL_5, ADC_IN5_RANK },
#endif
#ifdef ADC_IN6_RANK
		{	ADC_CHANNEL_6, ADC_IN6_RANK},
#endif
#ifdef ADC_IN7_RANK
		{	ADC_CHANNEL_7, ADC_IN7_RANK},
#endif
#ifdef ADC_IN8_RANK
		{	ADC_CHANNEL_8, ADC_IN8_RANK},
#endif
#ifdef ADC_IN9_RANK
		{	ADC_CHANNEL_9, ADC_IN9_RANK},
#endif
#ifdef ADC_IN10_RANK
		{ ADC_CHANNEL_10, ADC_IN10_RANK },
#endif
#ifdef ADC_IN11_RANK
		{	ADC_CHANNEL_11, ADC_IN11_RANK},
#endif
#ifdef ADC_IN12_RANK
		{ ADC_CHANNEL_12, ADC_IN12_RANK },
#endif
#ifdef ADC_IN13_RANK
		{	ADC_CHANNEL_13, ADC_IN13_RANK},
#endif
#ifdef ADC_IN14_RANK
		{	ADC_CHANNEL_14, ADC_IN14_RANK},
#endif
#ifdef ADC_IN15_RANK
		{ ADC_CHANNEL_15, ADC_IN15_RANK },
#endif
		};

#define ADC_NUMBER_OF_REG_CONV	COUNTOF(adc_regular_channels_ranks)

__IO uint16_t adcbuf[ADC_NUMBER_OF_REG_CONV];

#ifdef ADC_IN0_RANK
#define get_in0() (adcbuf[ADC_IN0_RANK - 1])
#endif
#ifdef ADC_IN1_RANK
#define get_in1() (adcbuf[ADC_IN1_RANK - 1])
#endif
#ifdef ADC_IN2_RANK
#define get_in2() (adcbuf[ADC_IN2_RANK - 1])
#endif
#ifdef ADC_IN3_RANK
#define get_in3() (adcbuf[ADC_IN3_RANK - 1])
#endif
#ifdef ADC_IN4_RANK
#define get_in4() (adcbuf[ADC_IN4_RANK - 1])
#endif
#ifdef ADC_IN5_RANK
#define get_in5() (adcbuf[ADC_IN5_RANK - 1])
#endif
#ifdef ADC_IN6_RANK
#define get_in6() (adcbuf[ADC_IN6_RANK - 1])
#endif
#ifdef ADC_IN7_RANK
#define get_in7() (adcbuf[ADC_IN7_RANK - 1])
#endif
#ifdef ADC_IN8_RANK
#define get_in8() (adcbuf[ADC_IN8_RANK - 1])
#endif
#ifdef ADC_IN9_RANK
#define get_in9() (adcbuf[ADC_IN9_RANK - 1])
#endif
#ifdef ADC_IN10_RANK
#define get_in10() (adcbuf[ADC_IN10_RANK - 1])
#endif
#ifdef ADC_IN11_RANK
#define get_in11() (adcbuf[ADC_IN11_RANK - 1])
#endif
#ifdef ADC_IN12_RANK
#define get_in12() (adcbuf[ADC_IN12_RANK - 1])
#endif
#ifdef ADC_IN13_RANK
#define get_in13() (adcbuf[ADC_IN13_RANK - 1])
#endif
#ifdef ADC_IN14_RANK
#define get_in14() (adcbuf[ADC_IN14_RANK - 1])
#endif
#ifdef ADC_IN15_RANK
#define get_in15() (adcbuf[ADC_IN15_RANK - 1])
#endif


//#define ADC_SAMPLE_G0 		get_in0
#define ADC_SAMPLE_G1 			get_in1
//#define ADC_SAMPLE_G2 		get_in2
#define ADC_SAMPLE_G3 			get_in3
#define ADC_SAMPLE_G4			get_in5
#define ADC_SAMPLE_U_RAIL 		get_in10
#define ADC_SAMPLE_U_SC			get_in12
#define ADC_SAMPLE_U_BEMF_REF 	adc_get_inj1
#define ADC_SAMPLE_U_BEMF		adc_get_inj2
#define ADC_SAMPLE_I			adc_get_inj3
#define ADC_SAMPLE_I2			adc_get_inj4

#endif /* ADC1_H_ */
