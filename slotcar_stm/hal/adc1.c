#include "adc1.h"

ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

uint32_t adc_inj_channels[] = {
	#ifdef ADC_INJ_CHANNELS
			ADC_INJ_CHANNELS
	#else
		#error ADC, channels for injected conversions are not defined
	#endif
};

#define ADC_NUMBER_OF_INJ_CONV	COUNTOF(adc_inj_channels)

void adc_init(void){
		ADC_ChannelConfTypeDef sConfig;
		ADC_InjectionConfTypeDef sConfigInjected;
		/* Peripheral clock enable */
		__ADC1_CLK_ENABLE();

		/*##-1- Configure the ADC peripheral #######################################*/
		hadc1.Instance = ADC1;
		hadc1.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
		hadc1.Init.Resolution = ADC_RESOLUTION_12B;
		hadc1.Init.ScanConvMode = ENABLE;
		hadc1.Init.ContinuousConvMode = ENABLE;
		hadc1.Init.NbrOfConversion = ADC_NUMBER_OF_REG_CONV; //TODO: Zkontrolovat
		hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
		hadc1.Init.EOCSelection = DISABLE; //end of conversion

		hadc1.Init.DiscontinuousConvMode = DISABLE;
		hadc1.Init.NbrOfDiscConversion = 0;

		hadc1.Init.DMAContinuousRequests = ENABLE;
		hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE; // Regular conversion is started by SW, not by external trigger
		hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1; //This is not used.
		HAL_ADC_Init(&hadc1);

		sConfig.SamplingTime = ADC_SAMPLETIME_56CYCLES;
		sConfig.Offset = 0;


		for(uint8_t i = 0; i < ADC_NUMBER_OF_REG_CONV; i++){
			sConfig.Channel = adc_regular_channels_ranks[i].channel;
			sConfig.Rank = adc_regular_channels_ranks[i].rank;
			HAL_ADC_ConfigChannel(&hadc1, &sConfig);
		}

		sConfigInjected.InjectedNbrOfConversion = ADC_NUMBER_OF_INJ_CONV;
		sConfigInjected.InjectedSamplingTime = ADC_SAMPLETIME_15CYCLES;
		sConfigInjected.ExternalTrigInjecConvEdge = ADC_EXTERNALTRIGINJECCONVEDGE_RISINGFALLING;
		sConfigInjected.ExternalTrigInjecConv = ADC_EXTERNALTRIGINJECCONV_T1_TRGO;
		sConfigInjected.AutoInjectedConv = DISABLE; //check
		sConfigInjected.InjectedDiscontinuousConvMode = DISABLE;
		sConfigInjected.InjectedOffset = 0;

		for(uint32_t i = 0; i < ADC_NUMBER_OF_INJ_CONV; i++){
			sConfigInjected.InjectedChannel = adc_inj_channels[i];
			sConfigInjected.InjectedRank = i+1; //Ranks start from 1
			HAL_ADCEx_InjectedConfigChannel(&hadc1, &sConfigInjected);
		}
		HAL_ADCEx_InjectedStart_IT(&hadc1);
		HAL_ADC_Start_DMA(&hadc1,(uint32_t*)&adcbuf, ADC_NUMBER_OF_REG_CONV);

}

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO clock */
  __GPIOA_CLK_ENABLE();
  __GPIOC_CLK_ENABLE();
  /* ADC1 Periph clock enable */
  __ADC1_CLK_ENABLE();
  /* Enable DMA2 clock */
  __DMA2_CLK_ENABLE();

  /* ADC GPIO pin configuration */
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Pin =  GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4| GPIO_PIN_5;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  GPIO_InitStruct.Pin =  GPIO_PIN_1 |  GPIO_PIN_3 | GPIO_PIN_5 ;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);



  /*##-3- Configure the DMA streams ##########################################*/
  /* Set the parameters to be configured */

	hdma_adc1.Instance = DMA2_Stream0;
	hdma_adc1.Init.Channel = DMA_CHANNEL_0;
	hdma_adc1.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_adc1.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_adc1.Init.MemInc = DMA_MINC_ENABLE;
	hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_adc1.Init.Mode = DMA_CIRCULAR;
	hdma_adc1.Init.Priority = DMA_PRIORITY_HIGH;
	hdma_adc1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	hdma_adc1.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
	hdma_adc1.Init.MemBurst = DMA_MBURST_SINGLE;
	hdma_adc1.Init.PeriphBurst = DMA_PBURST_SINGLE;
	HAL_DMA_Init(&hdma_adc1);

	__HAL_LINKDMA(hadc,DMA_Handle,hdma_adc1);

	HAL_NVIC_SetPriority(ADC_IRQn, 2, 2);
	HAL_NVIC_EnableIRQ(ADC_IRQn);

//	HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 2, 1);
//	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
}

/**
  * @brief ADC MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO to their default state
  * @param hadc: ADC handle pointer
  * @retval None
  */
void HAL_ADC_MspDeInit(ADC_HandleTypeDef *hadc)
{

  /*##-1- Reset peripherals ##################################################*/
  /* ADC1 Periph clock enable */
  __ADC1_CLK_DISABLE();
  /* Enable DMA2 clock */
  __DMA2_CLK_DISABLE();

  /*##-2- Disable peripherals and GPIO Clocks ################################*/
  /* De-initialize the ADC3 Channel8 GPIO pin */
  HAL_GPIO_DeInit(GPIOC, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);
  HAL_GPIO_DeInit(GPIOA, GPIO_PIN_1 | GPIO_PIN_3 | GPIO_PIN_5);


  /*##-3- Disable the DMA Streams ############################################*/
  /* De-Initialize the DMA Stream associate to transmission process */
  HAL_DMA_DeInit(hadc->DMA_Handle);

  /*##-4- Disable the NVIC for DMA ###########################################*/
  HAL_NVIC_DisableIRQ(DMA2_Stream0_IRQn);
}


//void HAL_ADC_DMAConvCplt(ADC_HandleTypeDef* hadc)
//{
//	//TODO: add code
//	return;
//}
//
//void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
//{
//	//TODO: add code
//	return;
//}
//
//void HAL_ADC_ErrorCallback(ADC_HandleTypeDef* hadc)
//{
//	//TODO: add code
//	return;
//}
//
//void HAL_ADCEx_InjectedErrorCallback(ADC_HandleTypeDef* hadc)
//{
//	//TODO: implement
//	return;
//}




