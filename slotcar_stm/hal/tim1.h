/*
 * Initialize TIM1 as a PWM generator.
 * PWM center-aligned mode on channel 1, 2, 3, 4 connected to pins PA8, PA9, PA10, PA11.
 */
#ifndef TIM1_H_
#define TIM1_H_

#include <stdint.h>
#include "stm32f4xx.h"

void tim1_pwm_init(uint32_t driver_period, uint32_t complementary_pwm);

extern TIM_HandleTypeDef htim1;

#endif /* TIM1_H_ */
