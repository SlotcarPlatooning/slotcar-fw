#ifndef __F4COMMON_H__
#define __F4COMMON_H__

#include "stm32f4xx.h"

#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

#define max(_a_,_b_)				(((_a_)<(_b_))?(_b_):(_a_))
#define min(_a_,_b_)				(((_a_)<(_b_))?(_a_):(_b_))
#define saturate(_val_,_min_,_max_)	((_val_)=(min(max(_val_,_min_),_max_)))
#define in_limits(a,low,high) 		(((a)<(high))&&((a)>(low)))


#define GPIO_INIT(gpio,pin,mode,otype,ospeed,pupd) ({	gpio->MODER   &= ~(3<<(pin*2)); gpio->MODER  |=(((mode)&3)  <<(pin*2));  \
							gpio->OTYPER  &= ~(1<<(pin));   gpio->OTYPER |=(((otype)&1) <<(pin));    \
							gpio->OSPEEDR &= ~(3<<(pin*2)); gpio->OSPEEDR|=(((ospeed)&3)<<(pin*2));  \
							gpio->PUPDR   &= ~(3<<(pin*2)); gpio->PUPDR  |=(((pupd)&3)  <<(pin*2));  })
							
#define GPIO_INIT_AF(gpio,pin,af) ({  if((pin)<8) { gpio->AFR[0] &= ~(15<<((pin)*4));      gpio->AFR[0] |= (((af)&15)<<((pin)*4)); } \
				      else        { gpio->AFR[1] &= ~(15<<(((pin)-8)*4));  gpio->AFR[1] |= (((af)&15)<<(((pin)-8)*4)); } })



#define SYSCFG_INIT_EXTI(EXTI_PortSourceGPIOx,EXTI_PinSourcex)  ({  SYSCFG->EXTICR[EXTI_PinSourcex >> 2] &= ~(((uint32_t)0x0F)                 << (4 * (EXTI_PinSourcex & (uint8_t)0x03)));   \
				        			    SYSCFG->EXTICR[EXTI_PinSourcex >> 2] |= ( ((uint32_t)EXTI_PortSourceGPIOx) << (4 * (EXTI_PinSourcex & (uint8_t)0x03)));      }  )

#define __enable_irq()               ({ asm volatile ("cpsie i"); })
#define __disable_irq()              ({ asm volatile ("cpsid i"); })

#define NVIC_ENABLE(IRQ)     NVIC->ISER[IRQ >> 0x05] =  (uint32_t)0x01 << (IRQ & (uint8_t)0x1F);
#define NVIC_DISABLE(IRQ)    NVIC->ICER[IRQ >> 0x05] =  (uint32_t)0x01 << (IRQ & (uint8_t)0x1F);

#define NVIC_SET_PENDING(IRQ)      NVIC->ISPR[IRQ >> 0x05] =  (uint32_t)0x01 << (IRQ & (uint8_t)0x1F);
#define NVIC_CLEAR_PENDING(IRQ)    NVIC->ICPR[IRQ >> 0x05] =  (uint32_t)0x01 << (IRQ & (uint8_t)0x1F);

#define NVIC_SET_PRIORITY(IRQ, prio)  	NVIC->IP[IRQ] = prio;

#define SPIx_transfer_byte(SPI,b)     ({  SPI->DR=b;  while (!(SPI->SR & SPI_FLAG_RXNE));  while (!(SPI->SR & SPI_FLAG_TXE)); SPI->DR;  })

//setup interrupt priority grouping
#define AIRCR_VECTKEY_MASK    ((uint32_t)0x05FA0000)
#define SET_IRQ_PRIGROUP(a)	SCB->AIRCR = AIRCR_VECTKEY_MASK | (0b011 << 8);



#define PORT_CLK_CONNECT(port) ({switch((uint16_t)port){        \
      case (uint16_t)GPIOA: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;  \
                            break;                                \
      case (uint16_t)GPIOB: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;  \
                            break;                                \
      case (uint16_t)GPIOC: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;  \
                            break;                                \
      case (uint16_t)GPIOD: RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;  \
                            break;                                \
      case (uint16_t)GPIOE: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;  \
                            break;                                \
      case (uint16_t)GPIOF: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;  \
                            break;                                \
      case (uint16_t)GPIOG: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;  \
                            break;                                \
      case (uint16_t)GPIOH: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;  \
                            break;                                \
      case (uint16_t)GPIOI: RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;  \
                            break;}})



#define iwdg_init(ps, rl)			\
{                                               \
  IWDG->KR = IWDG_WriteAccess_Enable;           \
  IWDG->PR = ps;                                \
  IWDG->RLR = rl;                               \
  IWDG->KR = KR_KEY_Reload;                     \
  IWDG->KR = KR_KEY_Enable;                     \
}

#define iwdg_reset() IWDG->KR = ((uint16_t)0xAAAA);


#define systick_init(rl)					                                                                   \
{                                                                                                                                  \
  SysTick->VAL = 0;							  	/* set reload value to determine the period  */    \
  SysTick->LOAD = rl; 							  	/* set reload value to determine the period  */    \
  SysTick->CTRL = (SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk); 	/* enable timer and its interrupt - value is */    \
}

#define systick_ovf() (SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk)


#endif
