#include <math.h>
#include "speed.h"
#include "model.h"
#include "adc1.h"
#include "main.h"
#include "stm_memory.h"
#include "irc.h"
#include "motor.h"
#include "filters.h"



#define CREATE_LOOKUP(name, _size) static float name_x[_size], name_y[_size]; static lookup_t name = {.x=name_x, .y=name_y, .size=_size}

// DO NOT CHECK SIZE...
#define CREATE_LOOKUP_XY(name, x_data, y_data) static lookup_t name = {.x=x_data, .y=y_data, .size=COUNTOF(x_data)}

//#define ARRAY(...) __VA_ARGS__
//#define CREATE_LOOKUP_XY(name, x_data, y_data) static float name_x[] = x_data; static float name_y[] = y_data; static lookup_t name = {.x=name_x, .y=name_y, .size=COUNTOF(name_x)}
//CREATE_LOOKUP_XY(R_bemf,ARRAY({0.0f,2.0f}),ARRAY({1e2f,1e4f}));

typedef struct {
	float* x;
	float* y;
	size_t size;
} lookup_t;

static float interpolation(float x, float x0, float x1, float y0, float y1){
	return y0 + (y1 - y0)/(x1 - x0) * (x - x0);
}

static float look_up(lookup_t* l, float speed){
	speed = fabsf(speed);
	int i;

	if( speed < l->x[0] ) {
		return l->y[0];
	}

	if( speed > l->x[l->size-1]) {
		return l->y[l->size-1];
	}

	for(i = 0; i < (int) l->size; i++){
		if(speed < l->x[i]){
			i -= 1;
			break;
		}
	}
	return interpolation(speed, l->x[i], l->x[i+1], l->y[i], l->y[i+1]);
}

//CREATE_LOOKUP(R_bemf,20);
// nizsi cislo  -> verime mereni
// vyssi cislo -> verime modelu
static float bemf_x[] = {0.0f,1.0f,1.5f};
static float bemf_y[] = {1e-1f,1e2f,1e3f};
CREATE_LOOKUP_XY(R_bemf, bemf_x, bemf_y);

static float irc_x[] = {0.0f,1.0f,2.0f};
static float irc_y[] = {1e4f,1e2f,1e-1f};
CREATE_LOOKUP_XY(R_irc, irc_x, irc_y);

#define nr	(CAR_N)*(WHEEL_RADIUS)
#define k_Rnr (MOTOR_K)/(MOTOR_R)/(nr)
#define mnr (CAR_M)*(nr)
#define k_mnr (MOTOR_K)/(mnr)
#define Rmnr (MOTOR_R)*(mnr)
#define k_Rmnr (MOTOR_K)/(Rmnr)
#define k2_Rmn2r2 (MOTOR_K)*(k_Rmnr)/(nr)

#define SIGMA_IRC(x) (0.0062f*(x)*(x)+0.095f*(x)+ 3.9E2f)
#define SIGMA_BEMF(x) (0.034f*(x)*(x)-21.0f*(x)+ 5.9E3f)

#define process_noise (10.0f)

static volatile float estimated_friction = 0; // friction estimated by the KF
static volatile float traveled_distance;

/*
 * Returns predicted friction by the KF.
 */
float speed_get_estimated_friction(){
	return estimated_friction;
}

/*
 * Calculates the prediction of the model.
 * @param v 	speed
 * @param u 	input voltage
 * @param friction_force (predicted)
 */
static inline float model(float v, float friction_force, float u) {
	float i = u / (MOTOR_R) - (k_Rnr) * v;
	return (k_mnr)*i - friction_force/(CAR_M);
}

float speed_kalman2(float speed_irc, float speed_bemf, float u, float h) {

static float Pk[2][2] = { { 1.0f * process_noise, 0 }, { 0,  1.0f * process_noise } };
/* Covariance matrix of x(t)*/
static const float Qd[2][2] = { { process_noise, 0.0f }, { 0.0f, process_noise } };
//static const float Qd[2][2] = { { process_noise, 0.0f }, { 0.0f, process_noise } };
/* Covariance matrix of y(t)*/
static float Rd[2][2];
static float Cd[2][2] = { { 1.0f, 0.0f }, { 1.0f, 0.0f } };
static float xk[2][1];
static float Kk[2][2];

	float v = xk[0][0];
	float friction_force = xk[1][0];


	float Ad[2][2] = { {1.0f - (k2_Rmn2r2)*h, -h/(CAR_M) }, { 0.0f, 1.0f } };


	//estimation of friction force - commented for test purposes
//	if (speed_mm >= 800.0f) {
//		Rd[0][0] = 1.0f*(SIGMA_IRC(speed_mm));
//	} else {
//		Rd[0][0] = 4430.0f;
//	}
//	Rd[0][0] /= 1.0E6f;


	Rd[0][0] = look_up(&R_irc,xk[0][0]);
	Rd[1][1] = look_up(&R_bemf,xk[0][0]);


	xk[0][0] = xk[0][0] + model(v, friction_force, u) * h;
		//Pk = Ad*Pk*Ad' + Qd;
		Pk[0][0] = Qd[0][0] + Ad[0][0] * (Ad[0][0] * Pk[0][0] + Ad[0][1] * Pk[1][0])+ Ad[0][1] * (Ad[0][0] * Pk[0][1] + Ad[0][1] * Pk[1][1]);
		Pk[0][1] = Qd[0][1] + Ad[1][0] * (Ad[0][0] * Pk[0][0] + Ad[0][1] * Pk[1][0])+ Ad[1][1] * (Ad[0][0] * Pk[0][1] + Ad[0][1] * Pk[1][1]);
		Pk[1][0] = Qd[1][0] + Ad[0][0] * (Ad[1][0] * Pk[0][0] + Ad[1][1] * Pk[1][0])+ Ad[0][1] * (Ad[1][0] * Pk[0][1] + Ad[1][1] * Pk[1][1]);
		Pk[1][1] = Qd[1][1] + Ad[1][0] * (Ad[1][0] * Pk[0][0] + Ad[1][1] * Pk[1][0])+ Ad[1][1] * (Ad[1][0] * Pk[0][1] + Ad[1][1] * Pk[1][1]);

		//Kk = Pk*Cd'/(Cd*Pk*Cd'+Rd);

		float a = (Rd[0][0] * Rd[1][1] - Rd[0][1] * Rd[1][0]+ Cd[1][0] * Cd[1][0] * Pk[0][0] * Rd[0][0]+ Cd[0][0] * Cd[0][0] * Pk[0][0] * Rd[1][1]+ Cd[1][1] * Cd[1][1] * Pk[1][1] * Rd[0][0]+ Cd[0][1] * Cd[0][1] * Pk[1][1] * Rd[1][1]+ Cd[0][0] * Cd[0][0] * Cd[1][1] * Cd[1][1] * Pk[0][0] * Pk[1][1]- Cd[0][0] * Cd[0][0] * Cd[1][1] * Cd[1][1] * Pk[0][1] * Pk[1][0]	+ Cd[0][1] * Cd[0][1] * Cd[1][0] * Cd[1][0] * Pk[0][0] * Pk[1][1]- Cd[0][1] * Cd[0][1] * Cd[1][0] * Cd[1][0] * Pk[0][1] * Pk[1][0]- Cd[0][0] * Cd[1][0] * Pk[0][0] * Rd[0][1]+ Cd[0][0] * Cd[0][1] * Pk[0][1] * Rd[1][1]	- Cd[0][1] * Cd[1][0] * Pk[0][1] * Rd[0][1]	- Cd[0][0] * Cd[1][0] * Pk[0][0] * Rd[1][0]	+ Cd[0][0] * Cd[0][1] * Pk[1][0] * Rd[1][1]	- Cd[0][0] * Cd[1][1] * Pk[0][1] * Rd[1][0]	- Cd[0][0] * Cd[1][1] * Pk[1][0] * Rd[0][1]	+ Cd[1][0] * Cd[1][1] * Pk[0][1] * Rd[0][0]	- Cd[0][1] * Cd[1][1] * Pk[1][1] * Rd[0][1]	- Cd[0][1] * Cd[1][0] * Pk[1][0] * Rd[1][0]	+ Cd[1][0] * Cd[1][1] * Pk[1][0] * Rd[0][0]	- Cd[0][1] * Cd[1][1] * Pk[1][1] * Rd[1][0]	- 2 * Cd[0][0] * Cd[0][1] * Cd[1][0] * Cd[1][1] * Pk[0][0]* Pk[1][1]+ 2 * Cd[0][0] * Cd[0][1] * Cd[1][0] * Cd[1][1] * Pk[0][1]* Pk[1][0]);
		Kk[0][0] = (Cd[0][0] * Pk[0][0] * Rd[1][1] + Cd[0][1] * Pk[0][1] * Rd[1][1]	- Cd[1][0] * Pk[0][0] * Rd[1][0] - Cd[1][1] * Pk[0][1] * Rd[1][0]+ Cd[0][0] * Cd[1][1] * Cd[1][1] * Pk[0][0] * Pk[1][1]- Cd[0][0] * Cd[1][1] * Cd[1][1] * Pk[0][1] * Pk[1][0]- Cd[0][1] * Cd[1][0] * Cd[1][1] * Pk[0][0] * Pk[1][1]+ Cd[0][1] * Cd[1][0] * Cd[1][1] * Pk[0][1] * Pk[1][0]) / a;
		Kk[0][1] = -(Cd[0][0] * Pk[0][0] * Rd[0][1] + Cd[0][1] * Pk[0][1] * Rd[0][1]- Cd[1][0] * Pk[0][0] * Rd[0][0] - Cd[1][1] * Pk[0][1] * Rd[0][0]- Cd[0][1] * Cd[0][1] * Cd[1][0] * Pk[0][0] * Pk[1][1]+ Cd[0][1] * Cd[0][1] * Cd[1][0] * Pk[0][1] * Pk[1][0]+ Cd[0][0] * Cd[0][1] * Cd[1][1] * Pk[0][0] * Pk[1][1]- Cd[0][0] * Cd[0][1] * Cd[1][1] * Pk[0][1] * Pk[1][0]) / a;
		Kk[1][0] = (Cd[0][0] * Pk[1][0] * Rd[1][1] + Cd[0][1] * Pk[1][1] * Rd[1][1]	- Cd[1][0] * Pk[1][0] * Rd[1][0] - Cd[1][1] * Pk[1][1] * Rd[1][0]+ Cd[0][1] * Cd[1][0] * Cd[1][0] * Pk[0][0] * Pk[1][1]- Cd[0][1] * Cd[1][0] * Cd[1][0] * Pk[0][1] * Pk[1][0]- Cd[0][0] * Cd[1][0] * Cd[1][1] * Pk[0][0] * Pk[1][1]	+ Cd[0][0] * Cd[1][0] * Cd[1][1] * Pk[0][1] * Pk[1][0]) / a;
		Kk[1][1] = -(Cd[0][0] * Pk[1][0] * Rd[0][1] + Cd[0][1] * Pk[1][1] * Rd[0][1]- Cd[1][0] * Pk[1][0] * Rd[0][0] - Cd[1][1] * Pk[1][1] * Rd[0][0]- Cd[0][0] * Cd[0][0] * Cd[1][1] * Pk[0][0] * Pk[1][1]+ Cd[0][0] * Cd[0][0] * Cd[1][1] * Pk[0][1] * Pk[1][0]	+ Cd[0][0] * Cd[0][1] * Cd[1][0] * Pk[0][0] * Pk[1][1]- Cd[0][0] * Cd[0][1] * Cd[1][0] * Pk[0][1] * Pk[1][0]) / a;

		//xk = xk + Kk*(zk(t,2) - Cd*xk);
		xk[0][0] = xk[0][0]	- Kk[0][0] * (Cd[0][0] * xk[0][0] - speed_irc + Cd[0][1] * xk[1][0])    - Kk[0][1] * (Cd[1][0] * xk[0][0] - speed_bemf + Cd[1][1] * xk[1][0]);
		xk[1][0] = xk[1][0]	- Kk[1][0] * (Cd[0][0] * xk[0][0] - speed_irc + Cd[0][1] * xk[1][0])    - Kk[1][1] * (Cd[1][0] * xk[0][0] - speed_bemf + Cd[1][1] * xk[1][0]);
		//Pk = (Pk - Kk*Cd*Pk);
		Pk[0][0] = Pk[0][0] - Pk[0][0] * (Cd[0][0] * Kk[0][0] + Cd[1][0] * Kk[0][1])- Pk[1][0] * (Cd[0][1] * Kk[0][0] + Cd[1][1] * Kk[0][1]);
		Pk[0][1] = Pk[0][1] - Pk[0][1] * (Cd[0][0] * Kk[0][0] + Cd[1][0] * Kk[0][1])- Pk[1][1] * (Cd[0][1] * Kk[0][0] + Cd[1][1] * Kk[0][1]);
		Pk[1][0] = Pk[1][0] - Pk[0][0] * (Cd[0][0] * Kk[1][0] + Cd[1][0] * Kk[1][1])- Pk[1][0] * (Cd[0][1] * Kk[1][0] + Cd[1][1] * Kk[1][1]);
		Pk[1][1] = Pk[1][1] - Pk[0][1] * (Cd[0][0] * Kk[1][0] + Cd[1][0] * Kk[1][1])- Pk[1][1] * (Cd[0][1] * Kk[1][0] + Cd[1][1] * Kk[1][1]);

		estimated_friction = xk[1][0];
		return xk[0][0];
}

//This is implementation of Kalman filter for one speed sensor only.
float speed_kalman1(float speed, float u, float h) {
	static const float Qd[2][2] = { { process_noise, 0.0f }, { 0.0f, process_noise } };
	static const float Cd[1][2] = { { 1.0f, 0.0f } };

	float Ad[2][2] = { {1- (k2_Rmn2r2)*h, -h/(CAR_M) }, { 0, 1 } };
	static float Pk[2][2] = { { process_noise, 0.0f }, { 0.0f, process_noise } };
	static float Rd, xk[2][1], Kk[2][1];

	Rd = look_up(&R_bemf,speed);

	//xk = xk + modelfunkce(xk(1),xk(2),u(t,2))*Ts;
	xk[0][0] = xk[0][0] + model(xk[0][0], xk[1][0], u) * h;

	//Pk = Ad*Pk*Ad' + Qd;
	float a1 = Ad[0][0] * Pk[0][0] + Ad[0][1] * Pk[1][0];
	float a2 = Ad[1][0] * Pk[0][0] + Ad[1][1] * Pk[1][0];
	float a3 = Ad[0][0] * Pk[0][1] + Ad[0][1] * Pk[1][1];
	float a4 = Ad[1][0] * Pk[0][1] + Ad[1][1] * Pk[1][1];
	Pk[0][0] = Qd[0][0] + Ad[0][0] * a1 + Ad[0][1] * a3;
	Pk[0][1] = Qd[0][1] + Ad[1][0] * a1 + Ad[1][1] * a3;
	Pk[1][0] = Qd[1][0] + Ad[0][0] * a2 + Ad[0][1] * a4;
	Pk[1][1] = Qd[1][1] + Ad[1][0] * a2 + Ad[1][1] * a4;

	//Kk = Pk*Cd'/(Cd*Pk*Cd'+Rd);
	float b = Rd + Cd[0][0] * (Cd[0][0] * Pk[0][0] + Cd[0][1] * Pk[1][0])+ Cd[0][1] * (Cd[0][0] * Pk[0][1] + Cd[0][1] * Pk[1][1]);
	Kk[0][0] = (Cd[0][0] * Pk[0][0] + Cd[0][1] * Pk[0][1]) / b;
	Kk[1][0] = (Cd[0][0] * Pk[1][0] + Cd[0][1] * Pk[1][1]) / b;

	//xk = xk + Kk*(zk(t,2) - Cd*xk);
	float c = Cd[0][0] * xk[0][0] - speed + Cd[0][1] * xk[1][0];
	xk[0][0] = xk[0][0] - Kk[0][0] * c;
	xk[1][0] = xk[1][0] - Kk[1][0] * c;

	//Pk = (Pk - Kk*Cd*Pk);
	float d1 = Cd[0][0] * Kk[0][0];
	float d2 = Cd[0][0] * Kk[1][0];
	float d3 = Cd[0][1] * Kk[0][0];
	float d4 = Cd[0][1] * Kk[1][0];
	float e1 = Pk[0][0] - d1 * Pk[0][0]- d3 * Pk[1][0];
	float e2 = Pk[0][1] - d1 * Pk[0][1]- d3 * Pk[1][1];
	float e3 = Pk[1][0] - d2 * Pk[0][0]- d4 * Pk[1][0];
	float e4 = Pk[1][1] - d2 * Pk[0][1]- d4 * Pk[1][1];

	Pk[0][0] = e1;
	Pk[0][1] = e2;
	Pk[1][0] = e3;
	Pk[1][1] = e4;

	estimated_friction = xk[1][0];
	return xk[0][0];
}

float speed_update(const float s_irc, const float s_bemf) {
	float u = motor_get_pwm() * stm_out.v_rail;
	static const float fs = 1.0f /(SPEED_UPDATE_FREQUENCY);
	/*
	stm_out.speed_kalman = speed_kalman2(s_irc, s_bemf, u, fs);
	/*/
	stm_out.speed_kalman = speed_kalman1(s_bemf, u, fs);

	stm_out.friction = speed_get_estimated_friction();


	float speed;
	switch (stm_in.speed_meas_type) {
	case KALMAN:
		speed = stm_out.speed_kalman;
		break;
	case IRC:
		speed = s_irc;
		break;
	case BEMF:
		speed = s_bemf;
		break;
	default:
		speed = 0.0f;
	}

//	if(in_limits(s_irc, -0.1f, 0.1f)){
//		speed = 0.0f;
//	}
	traveled_distance += speed * (1.0f / SPEED_UPDATE_FREQUENCY);
	return speed;
}



