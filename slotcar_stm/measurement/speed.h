/*
 * Speed measurement and kalman filtering.
 */

#ifndef SPEED_H_
#define SPEED_H_

typedef enum {
	KALMAN = 0, IRC = 1, BEMF = 2
} SpeedOutput;

/*
 * Step of the Kalman filter for 2 sensors
 * @param u Applied voltage to the motor. In case of PWM it is duty cycle times rail voltage.
 * @param h Period.
 */
float speed_kalman2(float speed_irc, float speed_bemf, float u, float h);

/*
 * Step of the Kalman filter. One sensor only.
 * Same inputs as first.
 */
float speed_kalman1(float speed, float u, float h);

/*
 * Returns value of friction estimated by Kalman filter.
 */
float speed_get_estimated_friction(void);

/*
 * Updates speed measurements.
 * Also does kalman filter step.
 */
float speed_update(const float s_irc, const float s_bemf);

#endif /* SPEED_H_ */
