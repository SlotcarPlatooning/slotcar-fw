/*
 * File includes all methods for modulation and demodulation of LED signal, used in distance measurement.
 * It's part of Slotcar platooning project.
 * Modulation is done by turning off and on again the LED diode. Demodulation is done with asynchronous detecting method.
 *
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "stm32f4xx.h"

#include "distance.h"
#include "main.h"
#include "stm_memory.h"

#define FREQ_FRONT	1111
#define FREQ_BACK	1667
#define PRECALCULATED

#define pow2(a)	((a)*(a))
#define PI_F 3.14159265358979f
#define DISTANCE_DEMOD_PERIODS 20

typedef struct {
	GPIO_TypeDef* led_port;
	uint16_t led_pin;
	uint16_t distance_meas_val;
	uint32_t freq;
	volatile float* distance_demodulated; /**< Demodulated signal*/
	uint32_t count_mod; /**< Counter for back modulation and demodulation*/
	uint32_t distance_filter_counter_back; /**< Counter for demodulating filter*/
	float distance_sum_sin_back; /**< Sum of sine demodulating signal*/
	float distance_sum_cos_back; /**< Sum of cosine demodulating signal*/
	uint32_t distance_frequency_divider;
	size_t size;
	float* reference_function_sin; /**< Reference sine function for demodulating, field of sine values*/
	float* reference_function_cos; /**< Reference cosine function for demodulating, field of cosine values*/
	uint32_t distance_filter_counter; /**< Counter for demodulating filter*/
	float distance_sum_sin; /**< Sum of sine demodulating signal*/
	float distance_sum_cos; /**< Sum of cosine demodulating signal*/
	float step;
	float f_div;
	uint32_t u32_div;
//	uint32_t distance_transfer[31]; /**< Field of transfer function values*/
//	uint32_t distance_offset[31]; /**< Field of offset values, currently are not used*/
//	float distance_direction[31]; /**< Direction of approximated line, used in transfer function*/
//	float distance_origin[31]; /**< Origin used in transfer function*/
//	float distance_mm; /**< Distance to the next car [mm]*/
//	float distance_mm_filtred; /**< Distance to the next car, filtered [mm]*/
} distance_s;

/*
 * @warning PLEASE READ THIS NOTE
 * The front and rear sensor are not the physical front and rear distance board !
 * The front distance measurement includes front phototransistor and rear IR LED and vice versa.
 */
distance_s front = { .led_port = GPIOA, .led_pin = GPIO_PIN_0, .freq = FREQ_FRONT };
distance_s back = { .led_port = GPIOA, .led_pin = GPIO_PIN_2, .freq = FREQ_BACK };

/*
 * @brief Method for modulating and demodulating signal from LED diode.
 * @param distance_photo_front_value Value from front phototransistor
 */
static inline void distance_sensor_process(distance_s* sensor, uint16_t distance_photo_value) {

	sensor->distance_meas_val = distance_photo_value;
	/*
	 * Distance measuring - modulation of LED signal, demodulation of signal on phototransistor
	 */
	// LED modulation
	++sensor->count_mod;

	if (sensor->count_mod == sensor->distance_frequency_divider) {
		if (stm_in.dist_back_transmit_en)
			HAL_GPIO_TogglePin(sensor->led_port, sensor->led_pin); //the front LED should blink with the frequency at which the back LED demodulates
	}

	if (sensor->count_mod == 2 * sensor->distance_frequency_divider) {
		if (stm_in.dist_back_transmit_en)
			HAL_GPIO_TogglePin(sensor->led_port, sensor->led_pin); //the front LED should blink with the frequency at which the back LED demodulates
		sensor->count_mod = 0;
	}

	// LED demodulation - using sine
#ifdef PRECALCULATED
	float sin_val = sensor->reference_function_sin[sensor->count_mod];
	float cos_val = sensor->reference_function_cos[sensor->count_mod];
#else
	float sin_val = sinf(sensor->step * (float)sensor->count_mod);
	float cos_val = cosf(sensor->step * (float)sensor->count_mod);
#endif

	sensor->distance_sum_sin += (float) distance_photo_value * sin_val;
	sensor->distance_sum_cos += (float) distance_photo_value * cos_val;
	++sensor->distance_filter_counter;

	if (sensor->distance_filter_counter == sensor->u32_div ) {
		float foo = pow2(sensor->distance_sum_sin) + pow2(sensor->distance_sum_cos);
		*sensor->distance_demodulated = sqrtf(foo) / sensor->f_div;
		sensor->distance_sum_cos = 0;
		sensor->distance_sum_sin = 0;
		sensor->distance_filter_counter = 0;
	}
}

static inline void init(distance_s* sensor) {

	/*
	 * LED PIN initialization
	 */
	GPIO_InitTypeDef GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Pin = sensor->led_pin;
	HAL_GPIO_Init(sensor->led_port, &GPIO_InitStruct);
	HAL_GPIO_WritePin(sensor->led_port, sensor->led_pin, GPIO_PIN_SET);

	/*
	 * Variables initialization
	 */
	sensor->distance_frequency_divider = (PWM_IRQ_FREQUENCY) / sensor->freq / 2;
	sensor->size = 2 * sensor->distance_frequency_divider * sizeof(float);
	sensor->reference_function_sin = malloc(sensor->size);
	sensor->reference_function_cos = malloc(sensor->size);
	sensor->u32_div = 2 * sensor->distance_frequency_divider * DISTANCE_DEMOD_PERIODS;
	sensor->f_div = (float) sensor->u32_div ;

	/**
	 * Function will fill the \ref reference_function_sin_front, \ref reference_function_cos_front,
	 * \ref reference_function_sin_back, \ref reference_function_cos_back fields with sine and cosine values
	 * used in demodulation.
	 */
	//Filling the sin and cosine field for demodulation
	//sin(2*pi*f*t)
	//f = f_PWM/2/DIV_f
	//t = i/f_PWM
	sensor->step = PI_F / (float) sensor->distance_frequency_divider;

	for (uint32_t i = 0; i < (2 * sensor->distance_frequency_divider); i++) {
		sensor->reference_function_sin[i] = sinf(sensor->step * (float) i);
		sensor->reference_function_cos[i] = cosf(sensor->step * (float) i);
	}
}

void distance_init(void) {
	init(&back);
	init(&front);
	back.distance_demodulated = &stm_out.distance_back_raw;
	front.distance_demodulated = &stm_out.distance_front_raw;
}

void distance_process(uint16_t foto_value_front, uint16_t foto_value_back) {
	distance_sensor_process(&front, foto_value_front);
	distance_sensor_process(&back, foto_value_back);
}

//float get_demodulated_signal_front(void) {
//	return *front.distance_demodulated;
//}

//float get_demodulated_signal_back(void) {
//	return *back.distance_demodulated;
//}

