#ifndef DISTANCE_H_
#define DISTANCE_H_

/**
 * Initialization of needed field, variables, etc.
 */
void distance_init(void);

/**
 * Process the signals from phototransistors. Should be included in PWM interruption
 * @param foto_value_front Signal from front phototransistor
 * @param foto_value_back Signal from back phototransistor
 */
void distance_process(uint16_t foto_value_front, uint16_t foto_value_back);

/**
 * Count the final distance. Should be included in main loop.
 */
//void distance_compute(void);

/**
 * Getter for front distance
 * @return distance_mm_filtred_front Distance from front car [mm]
 */
//float get_distance_front(void);

/**
 * Getter for back distance
 * @return distance_mm_filtred_back Distance from behind car [mm]
 */
//float get_distance_back(void);

/**
 * @deprecated Getter for demodulated signal from phototranzistor. Should be used for calibration.
 * @return distance_demodulated_front: Demodulated signal from front phototransistor [-]
 */
//float get_demodulated_signal_front(void);

/**
 * @deprecated Getter for demodulated signal from phototransistor. Should be used for calibration.
 * @return distance_demodulated_front: Demodulated signal from back phototransistor [-]
 */
//float get_demodulated_signal_back(void);

//float get_distance_unfiltred_front(void);

//float get_distance_unfiltred_back(void);

#endif /* DISTANCE_H_ */
