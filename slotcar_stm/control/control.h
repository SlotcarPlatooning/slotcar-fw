#ifndef CONTROL_H_
#define CONTROL_H_

typedef enum {
	REG_TYPE_STOP 		= 0x0,
	REG_TYPE_SPEED 		= 0x1, // 0b01
	REG_TYPE_DISTANCE 	= 0x3, // 0b11
	REG_TYPE_PWM 		= 0x4,
} RegType;

/*
 * Initiates controllers.
 */
void control_init(float reg_speed_period);

/*
 * Sets PWM to 0 when reg is REG_TYPE_STOP.
 */
void control_stop_handler(void);

/*
 * Sets PWM to ref_pwm when reg is REG_TYPE_PWM
 */
void control_pwm_handler(void);

/*
 * Controls speed in order to achieve ref_speed when reg is REG_TYPE_SPEED.
 */
void control_speed_handler(void);

#endif /* CONTROL_H_ */
