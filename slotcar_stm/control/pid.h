#ifndef PID_H_
#define PID_H_

/*
 * Implementation of PID controller
 * https://www.mathworks.com/help/simulink/slref/pidcontroller.html
 */

// TODO: Add feed-forward.




typedef volatile struct pid_t {
	float input;
	float output;
	float Ts; 	// Sampling period
	// P
	float kp;
	// I
	float ki;
	float integrator_value;
	// TODO D
	float kd;
	float N; 	// Filtration coefficient
	float D_filter_value;
	float _pre_error;

	float kb;
	float back_calculation;

	// limits
	float min;
	float max;


	float (*step)(struct pid_t* pid, float error);
} pid_t;

#define PID_STEP(pid,error) pid.step(&pid,error)

float pid_step_parallel(pid_t* pid, float error);
float pid_step_ideal(pid_t* pid, float error);
float pi_step_parallel_clamping(pid_t* pid, float error);
float pid_step_parallel_clamping(pid_t* pid, float error);
float pid_step_parallel_clamping_filter(pid_t* pid, float error);


#endif /* PID_H_ */
