#include <math.h>
#include "led.h"
#include "control.h"
#include "pid.h"
#include "motor.h"
#include "stm_memory.h"
#include "speed.h"
#include "f4common.h"
#include "nonlinearities.h"

pid_t reg_speed = {
		.step = &pi_step_parallel_clamping,
		// P
		.kp = 2.0f,
		// I
		.ki = 10.0f,
		// saturation
		.min=-1.0f,
		.max=1.0f
};

static inline RegType control_get_reg(void) {
	return stm_in.reg;
}

static inline void control_set_reg(RegType type) {
	stm_in.reg = (int) type;
}

void control_init(float reg_speed_period) {
	// speed controller settings
	reg_speed.Ts = reg_speed_period;
	control_set_reg(REG_TYPE_STOP);
}

void control_stop_handler(void) {
	if (control_get_reg() == REG_TYPE_STOP) {
		motor_set_pwm(0.0f);
	}
}

void control_pwm_handler(void) {
	if (control_get_reg() & REG_TYPE_PWM) {
		motor_set_pwm(stm_in.ref_pwm);
	}
}

void control_speed_handler(void) {
	if (control_get_reg() & REG_TYPE_SPEED) {
		// calculate control error
		float e = stm_in.ref_speed - stm_out.speed;
		if(!in_limits(stm_in.ref_speed, - 0.05f, 0.05f)){
			float u = PID_STEP(reg_speed, e);
			saturate(u,-1.0,1.0);
			motor_set_pwm(u);
		}else{
			reg_speed.integrator_value = 0.0f;
			motor_set_pwm(0.0f);
		}
	} else {
		reg_speed.integrator_value = 0;
	}
}
