#include "pid.h"
#include "f4common.h"
#include "nonlinearities.h"
#include <math.h>

inline float uP(pid_t* pid, float error) {
	return pid->kp * error;
}

inline float uI(pid_t* pid, float error){
	float ret = pid->integrator_value;
	pid->integrator_value += pid->ki * error * pid->Ts;
	return ret;
}

inline float uI_clamping(pid_t* pid, float error, float uPD){
	float ret = pid->integrator_value;
	float u = uPD + ret;
	// clamping
	float foo = deadzoneTypeOne(u, pid->min, pid->max);
	int bar = (int) signf(foo);
	float pre_intergrator = pid->ki * error;
	int tmp = (int) signf(pre_intergrator);
	int decision = ( foo != 0.0f ) && (bar == tmp) ;
	if ( !decision ) {
		uI(pid, error);
	}
	return ret;
}

inline float uI_back_calculation(pid_t* pid, float error){
	pid->integrator_value += (pid->ki * error + pid->kb * pid->back_calculation)* pid->Ts  ;
	float before = pid->integrator_value;
	pid->back_calculation = min(max(pid->integrator_value,pid->min),pid->max) - before;
	return pid->integrator_value;
}

inline float uD(pid_t* pid, float error) {
	pid->_pre_error = error;
	return pid->kd * (error - pid->_pre_error) / pid->Ts;
}

inline float uD_filter(pid_t* pid, float error) {
	float ret = pid->N * (error * pid->kd - pid->D_filter_value);
	pid->D_filter_value += ret * pid->Ts;
	saturate(pid->D_filter_value, pid->min, pid->max);
	return ret;
}

float pi_step_parallel_clamping(pid_t* pid, float error) {
	pid->input = error;
	float u = 0.0f;
	u += uP(pid, error);
	u += uI_clamping(pid, error, u);
	pid->output = u;
	return pid->output;
}

float pid_step_parallel_clamping(pid_t* pid, float error) {
	pid->input = error;
	float u = 0.0f;
	u += uP(pid, error);
	u += uD(pid, error);
	u += uI_clamping(pid, error,u);
	pid->output = u;
	return pid->output;
}

float pid_step_parallel_clamping_filter(pid_t* pid, float error) {
	pid->input = error;
	float u = 0.0f;
	u += uP(pid, error);
	u += uD_filter(pid, error);
	u += uI_clamping(pid, error,u);
	pid->output = u;
	return pid->output;
}

float pid_step_parallel(pid_t* pid, float error) {
	pid->input = error;
	pid->output = uP(pid, error) + uI(pid, error) + uD(pid, error);
	return pid->output;
}

float pid_step_ideal(pid_t* pid, float error) {
	pid->input = error;
	pid->output = uP(pid, error + uI(pid, error) + uD(pid, error));
	return pid->output;
}

