#ifndef FILTERS_H_
#define FILTERS_H_

#include "f4common.h"

/*
 * LOWPASS FILTER
 */
typedef struct {
/*
% MATLAB CODE
% ###############
% T system time const
% Ts sampling period
s = tf('s');
C = 1 / (T * s + 1)
bode(C)
Cd = c2d(C,Ts,'zoh')
% Result is in form
% k /(z - a)
% y[n] = k * u[n-1] + a * y[n-1]
*/

	//y = k / (z-a) * u
	float u;
	float y;
	float alpha;
} f_lowpass_t;
#define LOWPASS_GET_ALPHA(RC,fs) (1.0f/(fs))/( (RC) + (1.0f/(fs)))
#define create_f_lowpass(name, RC, fs) static f_lowpass_t name = {.alpha=LOWPASS_GET_ALPHA(RC,fs)}

float f_lowpass_step(f_lowpass_t* f, float input);


/*
 * MOVING AVERAGE FILTER
 */

typedef struct {
	uint32_t size;
	float* field;
	uint32_t field_ptr;
	float input;
	float sum;
	float mean;
} f_moving_average_t;

#define create_f_moving_average(name,_size) static float name_field[_size]; static f_moving_average_t name = {.field=name_field, .size=_size}

float f_moving_average_step(f_moving_average_t* f, float input);

/*
 * MEDIAN FILTER
 */
typedef struct {
	uint32_t size;
	float* field;
	float* field2;
	uint32_t field_ptr;
	float input;
	float median;
} f_median_t;

#define create_f_median(name,_size) static float name1[_size],name2[_size]; static f_median_t name = {.field=name1, .field2=name2, .size=_size}

void f_median_push(f_median_t* f, float input);
float f_median_step(f_median_t* f);

#endif /* FILTERS_H_ */
