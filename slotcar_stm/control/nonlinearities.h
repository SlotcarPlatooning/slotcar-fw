#ifndef NONLINEARITIES_H_
#define NONLINEARITIES_H_

#include <math.h>

inline float deadzoneTypeOne(float u, float min, float max) {
	return (u >= min && u <= max) ? 0 : u;
}

inline float deadzoneTypeTwo(float u, float min, float max) {
	return (u >= min && u <= max) ? 0 : (u > max) ? u - max : u - min;
}

inline float signf(float f) {
	if (  f == 0.0f || isnanf(f) ){
		return f;
	}else {
		return copysignf(1.0f, f);
	}
}

#endif /* NONLINEARITIES_H_ */
