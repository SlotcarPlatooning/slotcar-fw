#include "filters.h"

#include <stdlib.h>
#include <string.h>

float f_lowpass_step(f_lowpass_t* f, float input){
	f->u = input;
//	f->y = f->k * (f->u) + (1.0f - f->alpha) * (f->y);
	f->y = f->y + f->alpha * (input - f->y);
	return f->y;
}

int compare(const void *a,const void *b);

int compare(const void *a,const void *b) {
	float *x = (float *) a;
	float *y = (float *) b;
	if ( *x >  *y ) {
		return 1;
	}else{
		return -1;
	}
}

void f_median_push(f_median_t* f, float input){
	f->input = input;
	f->field[f->field_ptr] = f->input;

	if(++f->field_ptr == f->size){
		f->field_ptr = 0;
	}
}

float f_median_step(f_median_t* f){

	memcpy(f->field2, f->field, sizeof(float) * f->size);
	qsort(f->field2,f->size,sizeof(float),compare);

	if (f->size % 2) {
		uint32_t foo = f->size / 2;
		f->median = (f->field2[foo] + f->field2[foo + 1]) / 2;
	}else{
		f->median = f->field2[f->size / 2 + 1];
	}

	return f->median;
}


float f_moving_average_step(f_moving_average_t* f, float input){
	f->input = input;
	if(++f->field_ptr == f->size) f->field_ptr = 0;

	f->sum -= f->field[f->field_ptr];
	f->field[f->field_ptr] = f->input;
	f->sum += f->field[f->field_ptr];

//	fmf->mean = (uint32_t)(fmf->sum/(uint64_t)fmf->size);

	f->mean = f->sum/(float)f->size;
	return f->mean;
}

