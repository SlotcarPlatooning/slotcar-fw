#include "irc.h"
#include "main.h"
#include "model.h"
#include "filters.h"

/*
 * RAW data - decision level (a border between black and white) and hysteresis
 */
#define IRC_HYSTERESIS		400
#define IRC_DECISION_LEVEL 	1200

/*
 * Minimal speed that is going to be measured [mm/s].
 * Note, that measuring on low speed may not be very accurate.
 */
#define IRC_MIN_SPEED	0.1f // Minimum speed that is detected, lower is considered as zero [m/s]
#define IRC_LINES_COUNT	6 	 // Number of stripes on the rotational disk.

/* IRC - PROGRAM PART*/

/*
 * MATH
 *
 * b...black
 * w...white
 *
 * T = T_b+ T_w
 * phi_bw = 2*pi / ((#b + #w)/2) = 4*pi/(#b + #w)
 * T = N / f
 * f...freq of the timer
 * N..# of counter ticks (int_count)
 * omega = (phi_bw * r) / T
 * omega = phi_bw * r * f / N
 */

#define PHI 			2.0f * 3.14f / (IRC_LINES_COUNT)
#define PHI_BW			2.0f * (PHI)
#define IRC_CONST		(IRC_CC) * (WHEEL_RADIUS) * (PHI_BW) * (PWM_IRQ_FREQUENCY)
#define IRC_MAX_TICKS	(uint32_t)((IRC_CONST)/(IRC_MIN_SPEED))

static const uint16_t LEVEL_DOWN = IRC_DECISION_LEVEL - IRC_HYSTERESIS;
static const uint16_t LEVEL_UP = IRC_DECISION_LEVEL + IRC_HYSTERESIS;

static volatile uint32_t color = 0; // starting color [white or black]
static volatile uint32_t int_count = 0; // Counter of interruptions
// Time of stay on both colors (B and W).
static volatile uint32_t incr_time = IRC_MAX_TICKS;
static volatile uint32_t cross_time1 = IRC_MAX_TICKS; // Time of stay on the one color (B or W).
static volatile uint32_t cross_time2 = IRC_MAX_TICKS; // Time of stay on the other color (B or W).

static volatile uint16_t sample_irc;
static volatile int is_new;

float IRC_speed_conv(int sign){
	/*
	 * If there is no change in color for a long time set output to MAX.
	 * Which says that car stops.
	 */
	float foo;
	if (incr_time < (IRC_MAX_TICKS)) {
		foo = (sign > 0 ? 1.0f : -1.0f ) * (IRC_CONST)/(float)incr_time;
	}else{
		foo = 0.0f;
	}

	return foo;
}

static inline void update(volatile uint32_t* cross_time){
	*cross_time = int_count;
	incr_time = cross_time2 + cross_time1;
	int_count = 0;
	color = !color;
	is_new++;
}

int IRC_is(void){
	return IS_READY(&is_new);
}

void IRC_step_IT_handler(volatile uint16_t ir_val) {
	sample_irc = ir_val;
// white = low voltage (low value of ir_val)
// black = high voltage (high value of ir_val)
	if (color && (ir_val < LEVEL_DOWN)) {
		update(&cross_time1);
	}else if (!color && (ir_val > LEVEL_UP)) {
		update(&cross_time2);
	}else if (int_count < (IRC_MAX_TICKS)) {
		int_count++;
	}else{
		incr_time = IRC_MAX_TICKS;
	}
}
