/*
 * TODO thermometer
 *
 */

/*
 * TODO GYRO and ACC data cannot be switched to big endian format.
 * It helps performance, although just slightly.
 * Uncomment to have data output as big endian.
 */
//#define ACC_GYRO_BIG_ENDIAN

/*
 * Gyroscope range selection.
 * [250, 500, 2000]
 */
#define GYRO_RANGE 250

/*
 * Accelerometer range selection.
 * [2, 4, 8, 16]
 */
#define ACC_RANGE 4

/*
 *
 */
#include "sensors_i2c.h"
#include "stm32f4xx.h"
#include "stm_memory.h"
#include "filters.h"

#define ARR_SIZE(x) (sizeof(x)/sizeof(x[0]))

extern I2C_HandleTypeDef hi2c1;

#if GYRO_RANGE == 250
#define GYRO_RES 0b00000000
#elif GYRO_RANGE == 500
#define GYRO_RES 0b00010000
#elif GYRO_RANGE == 2000
#define GYRO_RES 0b00100000
#else
#error "Wrong range for gyroscope."
#endif

#if ACC_RANGE == 2
#define ACC_RES 0b00000000
#elif ACC_RANGE == 4
#define ACC_RES 0b00010000
#elif ACC_RANGE == 8
#define ACC_RES 0b00100000
#elif ACC_RANGE == 16
#define ACC_RES 0b00110000
#else
#error "Wrong range for accelerometer."
#endif

typedef enum {
	LITTLE, BIG
} endian_t;

#ifdef ACC_GYRO_BIG_ENDIAN
#define BIG_E (1<<6)
#define ACC_GYRO_ENDIAN BIG
#else
#define BIG_E (0<<6)
#define ACC_GYRO_ENDIAN LITTLE
#endif

#define ACC_GYRO_MEM (0x80|0x28) // auto increase memory + memory address

typedef struct {
	f_lowpass_t x;
	f_lowpass_t y;
	f_lowpass_t z;
} xyz_filter_t;

static inline void filter(xyz_filter_t* filt, volatile float* out_x, volatile float* out_y, volatile float* out_z, float x, float y, float z) {
	*out_x = f_lowpass_step(&filt->x, x);
	*out_y = f_lowpass_step(&filt->y, y);
	*out_z = f_lowpass_step(&filt->z, z);
}

typedef struct {
	uint8_t mem_addr;
	uint8_t mem_val;
} i2c_init_t;

/* ACC MEM INIT */
i2c_init_t acc_init[] = {
		{ 0x20, 0x67 }, /* 0x27 CTRL_REG1_A (20h) */
		{ 0x21, 0x30 }, /* 0x27 CTRL_REG1_A (20h) */
		{ 0x23, BIG_E | ACC_RES | (1<<3)}, /* CTRL_REG4_A (23h) */
		{ 0x24, 0x40 }, /* CTRL_REG5_A (24h) */
		{ 0x2e, 0x80 }, /* FIFO_CTRL_REG_A (2eh) */
};

#define ACC_FS 100.0f
#define ACC_LOWPASS_RC 0.5f

xyz_filter_t acc_filter = {
		.x={.alpha=LOWPASS_GET_ALPHA(ACC_LOWPASS_RC,ACC_FS)},
		.y={.alpha=LOWPASS_GET_ALPHA(ACC_LOWPASS_RC,ACC_FS)},
		.z={.alpha=LOWPASS_GET_ALPHA(ACC_LOWPASS_RC,ACC_FS)}
};

#define GYRO_FS 100.0f
#define GYRO_LOWPASS_RC 0.5f
xyz_filter_t gyro_filter = {
		.x={.alpha=LOWPASS_GET_ALPHA(GYRO_LOWPASS_RC,GYRO_FS)},
		.y={.alpha=LOWPASS_GET_ALPHA(GYRO_LOWPASS_RC,GYRO_FS)},
		.z={.alpha=LOWPASS_GET_ALPHA(GYRO_LOWPASS_RC,GYRO_FS)}
};

/* GYRO MEM INIT */
i2c_init_t gyro_init[] = {
		{ 0x20, 0x0f }, /* 0x7f CTRL_REG1_G (20h) */
		{ 0x21, 0x22 }, /* CTRL_REG2_G (21h) */
		{ 0x23, BIG_E |GYRO_RES}, /* CTRL_REG4_G (23h) */
		{ 0x24, 0x12 }, /* CTRL_REG5_G (24h) */
	//	{ 0x2e, 0x00 }, /* FIFO_CTRL_REG_G (2eh) */
};

/* ACC MEM INIT */
i2c_init_t mag_init[] = {
		{0x10, 0x01 },
		{0x11, 0x00},
};

typedef struct{
	volatile float* x;
	volatile float* y;
	volatile float* z;
	float conv_c;
	endian_t endian;
	uint8_t dev_addr;
	uint8_t mem_addr;
	uint8_t raw[6];
	uint16_t init_size;
	i2c_init_t* init;
	xyz_filter_t *filt;
} i2c_xyz_t;

i2c_xyz_t sensors[] = {
		/*
		 * ACCELEROMETER
		 */
		{
			.dev_addr = (0x18<<1),
			.conv_c = 2.0f * (ACC_RANGE * 9.81f) / 65536.0f, //2*(scale[g]*g)/(2^16) = [mm/s^2 per digit]
			.mem_addr = ACC_GYRO_MEM,
			.x = &stm_out.acc_x,
			.y = &stm_out.acc_y,
			.z = &stm_out.acc_z,
			.endian = ACC_GYRO_ENDIAN,
			.init = acc_init,
			.init_size = ARR_SIZE(acc_init),
			.filt = &acc_filter
		},
		/*
		 * GYRO
		 */
		{
			.dev_addr = (0x6a<<1),
			.conv_c = 2.0f * (GYRO_RANGE * 3.14f / 180.0f) / 65536.0f, //2*(scale[dps]*pi/180)/(2^16) = [rad/s per digit]
			.mem_addr = ACC_GYRO_MEM,
			.x = &stm_out.gyro_pitch,
			.y = &stm_out.gyro_roll,
			.z = &stm_out.gyro_yaw,
			.endian = ACC_GYRO_ENDIAN,
			.init = gyro_init,
			.init_size = ARR_SIZE(gyro_init),
			.filt = &gyro_filter
		},
		/*
		 * MAGNETOMETER
		 */
		{
			.endian = BIG,
			.dev_addr = (0x0E<<1),
			.mem_addr = 0x01,
			.conv_c = 2.0f * 1000.0f / 65636.0f, // +- 1000 [microT] = [microT per digit]
			.x = &stm_out.mag_x,
			.y = &stm_out.mag_y,
			.z = &stm_out.mag_z,
			.init = mag_init,
			.init_size = ARR_SIZE(mag_init),
			.filt = NULL
		}
};

#define SENSORS_SIZE (sizeof(sensors)/sizeof(sensors[0]))

inline int i2c_mem_read(uint8_t dev_addr, uint8_t mem_addr, uint8_t *data, uint16_t size) {
	return HAL_I2C_Mem_Read_DMA(&hi2c1, (uint16_t) dev_addr, mem_addr,I2C_MEMADD_SIZE_8BIT, data, size);
}

inline int i2c_mem_write(uint8_t dev_addr, uint8_t mem_addr, uint8_t *data, uint16_t size) {
	return HAL_I2C_Mem_Write(&hi2c1, (uint16_t) dev_addr, mem_addr, I2C_MEMADD_SIZE_8BIT, data, size, 100);
}

inline int read_xyz(i2c_xyz_t* sensor){
	return i2c_mem_read(sensor->dev_addr, sensor->mem_addr, sensor->raw, 6);
}

int sensors_i2c_init(void) {
	for(uint16_t i = 0; i < SENSORS_SIZE; i++){
		i2c_xyz_t* sensor = &sensors[i];
		for(uint16_t j = 0; j < sensor->init_size; j++){
			i2c_init_t* init = &sensor->init[j];
			int ret = i2c_mem_write(sensor->dev_addr, init->mem_addr,&init->mem_val,1);
			if(ret != HAL_OK){
				return ret;
			}
		}
	}
	return HAL_OK;
}

static inline int16_t conv(uint8_t high, uint8_t low) {
	return (int16_t)((high << 8) | low);
}

static inline void sensor_conversion(i2c_xyz_t* sensor){
	float c = sensor->conv_c;

	float x , y ,z;

	if(sensor->endian == LITTLE ){
		x = c * conv(sensor->raw[1], sensor->raw[0]);
		y = c * conv(sensor->raw[3], sensor->raw[2]);
		z = c * conv(sensor->raw[5], sensor->raw[4]);
	}else{
		x = c * conv(sensor->raw[0], sensor->raw[1]);
		y = c * conv(sensor->raw[2], sensor->raw[3]);
		z = c * conv(sensor->raw[4], sensor->raw[5]);
	}
	// TODO filtration

	if( sensor->filt ){
		filter(sensor->filt, sensor->x, sensor->y, sensor->z, x, y, z);
	}else{
		*sensor->x = x;
		*sensor->y = y;
		*sensor->z = z;
	}
}


void sensors_read_start() {
	if(hi2c1.State != HAL_I2C_STATE_READY){
		return;
	}
	static int i = 0;
	static int prev = 0;
	i2c_xyz_t* sensor = &sensors[i];
	if(read_xyz(sensor) == HAL_OK){
		sensor_conversion(&sensors[prev]);
		prev = i;
		i++;
		if(i == SENSORS_SIZE){
			i = 0;
		}
	}
}


