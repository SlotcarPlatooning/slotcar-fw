/*
 * Calculates car's speed.
 * IRC-like measurement.
 * Binary color detection.
 */
#ifndef IRC_H_
#define IRC_H_

#include <stdint.h>

/*
 * Step of IRC measurement process.
 * Must be called periodically. For example in an interrupt.
 */
void IRC_step_IT_handler(volatile uint16_t ir_val);

/*
 * Converts RAW data to speed.
 */
float IRC_speed_conv(int sign);

/*
 * Is there a new sample?
 */
int IRC_is(void);

#endif /* IRC_H_ */
