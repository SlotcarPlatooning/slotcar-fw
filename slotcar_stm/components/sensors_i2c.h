/*
 * Reads out data from i2c sensors.
 * Using polling.
 * (1) Accelerometer
 * (2) Gyroscope
 * (3) Magnetometer
 */

#ifndef SENSORS_I2C_H_
#define SENSORS_I2C_H_

/*
 * Writes initialization settings to the devices.
 */
int sensors_i2c_init(void);

/*
 * Polling sensors.
 * On each call it tries to start new communication on i2c with a sensor.
 * When it succeed to start, it calls data conversion for previous sensor.
 * When i2c is busy it does nothing.
 */
void sensors_read_start(void);

#endif /* SENSORS_I2C_H_ */
