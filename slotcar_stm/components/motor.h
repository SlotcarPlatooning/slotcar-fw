
/*
 * Drives the DC motor through motor driver (H-bridge).
 * Measures BEMF and electrical current.
 * TIM1 PWM is used.
 * Lock Anti-Phase, Fast decay and slow decay modes are available.
 * http://www.modularcircuits.com/blog/articles/h-bridge-secrets/lock-anti-phase-drive/
 *
 * Vocabulary:
 * "i" stands for electrical current flowing through motor winding.
 */
#ifndef MOTOR_H_
#define MOTOR_H_

#include <stdint.h>

/*
 * PINS CONFIGURATION
 *
 * PA12 - NSLEEP
 * PA11 - INB - TIM1_CH4
 * PA10 - ENB - TIM1_CH3
 * PA9	- INA - TIM1_CH2
 * PA8	- ENA - TIM1_CH1
 *
 * 			ENA	ENB	INA	INB
 * Forward	1	1	1	0
 * Brake	1	1	0	0
 * Brake	1	1	1	1
 * Reverse	1	1	0	1
 * Coast	0	0	X	X
 * Coast	0	1	X	X
 * Coast	1	0	X	X
 */

/*
 * Choose on of following modes.
 */
#define LOCK_ANTIPHASE
//#define FAST_DECAY
//#define SLOW_DECAY

/*
 * Measurement settings
 */
#define BEMF_FREQ 500.0f
#define BEMF_TIME 0.0004f

/*
 * Initiates PWM for motor.
 */
void pwm_init(void);

/*
 * Measures i offset.
 * Must be called after PWM is initialized.
 */
void init_motor_i_offset(void);

/*
 * Enable or disable motor (puts it to sleep).
 */
void motor_sleep(uint32_t en);

/*
 * Sets PWM duty cycle.
 */
void motor_set_pwm(float pwm);

/*
 * Returns PWM duty cycle.
 */
float motor_get_pwm(void);

void motor_set_i_offset(float offset);


void motor_IRQ_handler(void);

/*
 * Is there a new measurement of I?
 */
int motor_is_i(void);

/*
 * Step of motor-electrical current measurement.
 * Called in PWM IRQ.
 * Measures in the half of PWM period which is bigger. Because of peaks on changes.
 * For dc < 0.5 it measures in the second half and vice versa.
 */
void motor_i_raw_handler(void);

/*
 * Is there a new measurement of BEMF?
 */
int motor_is_bemf(void);
/*
 * Step of motor BEMF measurement.
 * @deprecated
 */
void motor_bemf_raw_handler(void);



void motor_test(void);
#endif /* MOTOR_H_ */
