/*
 * led.c
 *
 *  Created on: 11. 5. 2015
 *      Author: Martin Lad
 */

#include "led.h"

void led_init(void){
	GPIO_InitTypeDef GPIO_InitStruct;

	LED_BLUE_CLK_ENABLE();
	LED_YELLOW_CLK_ENABLE();
	LED_RED_CLK_ENABLE();

	/* Global confoguration for LED pins */
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

	/* Configure yellow LED pin */
	GPIO_InitStruct.Pin = LED_YELLOW_PIN;
	HAL_GPIO_Init(LED_YELLOW_GPIO, &GPIO_InitStruct);

	/* Configure blue LED pin */
	GPIO_InitStruct.Pin = LED_BLUE_PIN;
	HAL_GPIO_Init(LED_BLUE_GPIO, &GPIO_InitStruct);

	/* Configure red LED pin */
	GPIO_InitStruct.Pin = LED_RED_PIN;
	HAL_GPIO_Init(LED_RED_GPIO, &GPIO_InitStruct);
}

inline void led_on(GPIO_TypeDef * gpio, uint16_t pin){
	HAL_GPIO_WritePin(gpio,pin,GPIO_PIN_SET);
}

inline void led_off(GPIO_TypeDef * gpio, uint16_t pin){
	HAL_GPIO_WritePin(gpio,pin,GPIO_PIN_RESET);
}

inline void led_toggle(GPIO_TypeDef * gpio, uint16_t pin){
	HAL_GPIO_TogglePin(gpio,pin);
}


void led_red_on(void){
	led_on(LED_RED_GPIO,LED_RED_PIN);
}
void led_blue_on(void){
	led_on(LED_BLUE_GPIO,LED_BLUE_PIN);
}
void led_yellow_on(void){
	led_on(LED_YELLOW_GPIO,LED_YELLOW_PIN);
}

void led_red_off(void){
	led_off(LED_RED_GPIO,LED_RED_PIN);
}
void led_blue_off(void){
	led_off(LED_BLUE_GPIO,LED_BLUE_PIN);
}
void led_yellow_off(void){
	led_off(LED_YELLOW_GPIO,LED_YELLOW_PIN);
}

void led_red_toggle(void){
	led_toggle(LED_RED_GPIO,LED_RED_PIN);
}
void led_blue_toggle(void){
	led_toggle(LED_BLUE_GPIO,LED_BLUE_PIN);
}
void led_yellow_toggle(void){
	led_toggle(LED_YELLOW_GPIO,LED_YELLOW_PIN);
}


