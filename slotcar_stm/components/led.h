#ifndef LED_H_
#define LED_H_

#include "stm32f4xx_hal.h"

#define LED_BLUE_CLK_ENABLE __GPIOC_CLK_ENABLE
#define LED_BLUE_GPIO 	GPIOC
#define LED_BLUE_PIN 	GPIO_PIN_7

#define LED_RED_CLK_ENABLE __GPIOC_CLK_ENABLE
#define LED_RED_GPIO	GPIOC
#define LED_RED_PIN 	GPIO_PIN_9

#define LED_YELLOW_CLK_ENABLE __GPIOC_CLK_ENABLE
#define LED_YELLOW_GPIO	GPIOC
#define LED_YELLOW_PIN 	GPIO_PIN_11

/*/

#define LED_CLK_ENABLE __GPIOD_CLK_ENABLE

#define LED_BLUE_GPIO 	GPIOD
#define LED_BLUE_PIN 	GPIO_PIN_15

#define LED_RED_GPIO	GPIOD
#define LED_RED_PIN 	GPIO_PIN_14

#define LED_YELLOW_GPIO	GPIOD
#define LED_YELLOW_PIN 	GPIO_PIN_13
*/

void led_init(void);

void led_yellow_on(void);
void led_red_on(void);
void led_blue_on(void);

void led_yellow_off(void);
void led_red_off(void);
void led_blue_off(void);

void led_yellow_toggle(void);
void led_red_toggle(void);
void led_blue_toggle(void);

#endif
