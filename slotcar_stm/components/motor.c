#include <filters.h>
#include "stm32f4xx.h"
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "motor.h"
#include "led.h"
#include "f4common.h"
#include "speed.h"
#include "tim1.h"
#include "adc1.h"
#include "main.h"
#include "stm_memory.h"
#include "filters.h"
#include "model.h"
/* Value to be set in ARR register to generate signal frequency at PWM_FREQUENCY */

#define IRQ_FREQ		PWM_IRQ_FREQUENCY
// 84MHz is frequency of APB2 @see configure_system_clock()
#define DRIVER_PERIOD	((84000000/(PWM_IRQ_FREQUENCY)) - 1) // 2*, because it uses central count mode
#define PWM_MAX 		((DRIVER_PERIOD)-(DRIVER_DT)-(DRIVER_BOOT))

#define DRIVER_DT		0		// dead-time
#define DRIVER_BOOT		0		//Boot time, minimum time to charge the bootstrap capacitors

#define TICKS_ON		(IRQ_FREQ*(BEMF_TIME))
#define TICKS_OFF 		((TICKS_ON) + (IRQ_FREQ)*(1.0f/(BEMF_FREQ) - (BEMF_TIME)))

/*
 * Measurements constants
 */
#define BEMF_MEASUREMENT_CONST	(16.5f/50.0f) // Amplifier constant
#define MOTOR_CURRENT_CONST 	(-1.0f/3.0f/0.2f) // Amplifier constant  = 1 / amplifier_const / meas_resistor


#if ! (defined(LOCK_ANTIPHASE) || defined(FAST_DECAY) || defined(SLOW_DECAY))
#error No motor-driver mode is selected.
#endif

#if defined(LOCK_ANTIPHASE) + defined(FAST_DECAY) + defined(SLOW_DECAY) != 1
#error Define exactly one of the motor-driver modes.
#endif

/*
 * The most recent samples.
 */
static volatile uint16_t i_sample = 0; // Raw sample.
static volatile float i_offset = 0.0f; // Measured offset while initialization.
static volatile int new_i = 0;  // True if new sample of i is available. Manual reset.
static volatile int new_bemf = 0;  // True if new sample of i is available. Manual reset.

static volatile uint16_t sample_motor_current2 = 0;
static volatile uint16_t sample_motor_bemf = 0;
static volatile uint16_t sample_motor_bemf_ref = 0;



static volatile int last_u = 0;
static volatile int enable = 1;

static inline int pwm_2_u(float pwm){
#ifdef LOCK_ANTIPHASE
	// PWM is <-1;1> however in this mode <-1;0> and <0;1> are the same.
	// The actual zero is -0.5 or 0.5.
	pwm /= 2.0f; // scaling
	pwm += 0.5f; // zero shift
#endif
	return (int)(PWM_MAX * pwm);
}

static inline void set_ab(int u) {
	last_u = u;
#if defined(SLOW_DECAY)
	if (u > 0) {
		TIM1->CCR1 = TIM1->CCR3 = 0xFFFF;
		TIM1->CCR2 = (uint32_t) (u + DRIVER_DT);
		TIM1->CCR4 = 0x0;
	} else {
		TIM1->CCR1 = TIM1->CCR3 = 0xFFFF;
		TIM1->CCR2 = 0x0;
		TIM1->CCR4 = (uint32_t) (-u + DRIVER_DT);
	}
#elif defined(LOCK_ANTIPHASE)
	uint32_t x = (uint32_t) ((u > 0)? u : -u);
	TIM1->CCR1 = TIM1->CCR3 = 0xFFFF;
	TIM1->CCR2 = x;
	TIM1->CCR4 = x + DRIVER_DT;
#elif defined(FAST_DECAY)
	if(u > 0) {
		TIM1->CCR2 = 0xFFFF;
		TIM1->CCR4 = 0x0;
		 TIM1->CCR3 = (uint32_t) (u + DRIVER_DT);
		 TIM1->CCR1 =  (uint32_t) (u );
	} else {
		TIM1->CCR2 = 0x0;
		TIM1->CCR4 = 0xFFFF;
		TIM1->CCR3 = (uint32_t)(-u + DRIVER_DT);
		TIM1->CCR1 =(uint32_t)(-u );
	}
#endif
}

static inline float get_pwm(void) {
	return stm_out.applied_pwm;
}

static inline void set_pwm(float pwm) {
	if (enable) {
		set_ab(pwm_2_u(pwm));
	}
	stm_out.applied_pwm = pwm;
}

static inline void set_coast_off(void){
#ifdef BREAKING
	TIM1->CCR1 = TIM1->CCR3 = 0xFFFF;
#else
	set_pwm(stm_out.applied_pwm);
#endif

}

static inline void set_coast_on(void) {
	TIM1->CCR1 = TIM1->CCR3 = 0x0;
//	TIM1->CCR2 = TIM1->CCR4 = 0x0; // coast does't depend on IN pins, only EN
}

//static inline void set_break(void) {
//	TIM1->CCR1 = TIM1->CCR3 = 0xFFFF;
//	TIM1->CCR2 = TIM1->CCR4 = 0x0;
//}

//static inline int pwm_level(void){
//#if defined(SLOW_DECAY)
//	int x = pwm_a_level() ? 1 : 0;
//	int y = pwm_b_level() ? 1 : 0;
//	return x ^ y;
//#elif defined(LOCK_ANTIPHASE)
//	return htim1.Instance->CNT > (htim1.Instance->ARR / 2);
//#elif defined(FAST_DECAY)
//	int x = pwm_a_is_on() ? 1 : 0;
//	int y = pwm_b_is_on() ? 1 : 0;
//	return x & y;
//return
//#endif
//}

static inline int is_long(void){
#if defined(SLOW_DECAY)
#error not implemented
	return 1;
#elif defined(LOCK_ANTIPHASE)
	uint32_t arr = (htim1.Instance->ARR / 2);
	int foo = (htim1.Instance->CNT > arr);
	int bar = (htim1.Instance->CCR2 > arr);
	if(!foo && bar){
		return -1;
	}else if (foo && !bar){
		return 1;
	}else{
		return 0;
	}
#elif defined(FAST_DECAY)
#error not implemented
	return 1;
#endif
}
//	Vbemf2 = (float) control_get_reference_pwm() / 100.0f * Vcc - motor_current * motor_R- motor_current * 0.2f - motor_current * 1.66f;
static inline float motor_bemf_sample_conv(void) {
	return (BEMF_CC) * adc_to_V(sample_motor_bemf - sample_motor_bemf_ref)/BEMF_MEASUREMENT_CONST;
}

static inline float motor_i_raw_conv(void) {
	return (I_CC) * (MOTOR_CURRENT_CONST) * (adc_to_V(i_sample - 4096/2)) - i_offset;
}

static inline float motor_current2_sample_conv(void) {
	return adc_to_V(sample_motor_current2) / 5.0f / 0.2f - 0.3f;
}

/*
 * Measures i.
 * We measure i only at the longer part of PWM period.
 * Because of measurements artifacts when duty cycle is really low or very high (< 10%).
 * */
void motor_i_raw_handler() {
		float foo = motor_i_raw_conv();
#ifdef I_FILT
		// median filter
		create_f_median(f_median_i, 30); // static
		f_median_push(&f_median_i,foo);
		foo = f_median_step(&f_median_i);
		// lowpass
		create_f_lowpass(f_lowpass_i, 0.4e-3, 20e3f); // static
		foo = f_lowpass_step(&f_lowpass_i, foo);
#endif
		stm_out.motor_i = foo;

#ifndef BEMF_MEASUREMENT_ENABLE
		new_bemf++;
#endif
}

void motor_bemf_raw_handler(void) {
#ifdef BEMF_MEASUREMENT_ENABLE
	float foo = motor_bemf_sample_conv();
#ifdef BEMF_FILT
	// median filter
	create_f_median(f_median_bemf, 3); // static
	f_median_push(&f_median_bemf,foo);
	foo = f_median_step(&f_median_bemf);
	// lowpass
	create_f_lowpass(f_lowpass_bemf, 5e-4, 500); // static
	foo = f_lowpass_step(&f_lowpass_bemf,foo);
#endif
	stm_out.motor_bemf = foo;
#else
	stm_out.motor_bemf = (BEMF_CC) * (stm_out.v_rail * motor_get_pwm() - (MOTOR_CURRENT_R + 0.2f + 1.0f)*stm_out.motor_i);
#endif
}

void pwm_init() {
	__GPIOA_CLK_ENABLE();

	GPIO_InitTypeDef GPIO_InitStruct;

	/* Sleep pin */
	GPIO_InitStruct.Pin = GPIO_PIN_12;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
#ifdef LOCK_ANTIPHASE
	tim1_pwm_init(DRIVER_PERIOD,1);
#else
	tim1_pwm_init(DRIVER_PERIOD,0);
#endif
	motor_sleep(0);
}

void motor_test(void) {

	GPIO_InitTypeDef GPIO_InitStruct;
	/* Clock Inicialization*/
	__HAL_RCC_GPIOA_CLK_ENABLE();
	/* GPIO Inicialization*/
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	/* PA8 - ENA, PA9 - INA, PA10 - ENB, PA11 - INB */
	GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
//	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	motor_sleep(0);
}

int motor_is_i(void){
	return IS_READY(&new_i);
}

int motor_is_bemf(void){
	return IS_READY(&new_bemf);
}

void motor_IRQ_handler(void){

	// I
	if(is_long() && enable){
		i_sample = ADC_SAMPLE_I();
		new_i++;
	}

	// BEMF
#ifdef BEMF_MEASUREMENT_ENABLE
	static uint32_t counterBemf = 0;
	counterBemf++;
	if (counterBemf == (uint32_t) TICKS_ON) {
		sample_motor_bemf = ADC_SAMPLE_U_BEMF();
		sample_motor_bemf_ref = ADC_SAMPLE_U_BEMF_REF();
		new_bemf++;
		enable = 1;
		set_coast_off();
	}

	if (counterBemf == (uint32_t) TICKS_OFF) {
		enable = 0;
		set_coast_on();
		counterBemf = 0;
	}
#endif
}

void motor_set_i_offset(float offset) {
	i_offset = offset;
}

void motor_set_pwm(float pwm) {
	set_pwm(pwm);
}

float motor_get_pwm(void) {
	return get_pwm();
}

void motor_sleep(uint32_t en) {
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, !en);
}
