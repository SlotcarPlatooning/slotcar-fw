/* New implementation of communication is sensor-like, base principle is writing data to register of reading data from register */

#include "communication.h"
#include "led.h"
#include "myvariable.h"


#define COM_RW_MASK			0b10000000
#define COM_AUTOINC_MASK	0x40
#define COM_ADDR_MASK		0b00111111

#define COM_READ			0x02
#define COM_WRITE			0x01

#define COM_RESP_ERR		0xFF
#define COM_RESP_OK			0x01

typedef enum {
	COM_STATE_IDLE = 1,
	COM_STATE_SYNC = 2,
	COM_STATE_LEN = 3,
	COM_STATE_REG_ADDR = 4,
	COM_STATE_WRITE = 5,
	COM_STATE_WRITE_RESP = 6,
	COM_STATE_WRITE_CRC = 7,
	COM_STATE_READ_RESP = 8,
	COM_STATE_READ_LEN = 9,
	COM_STATE_READ_DATA = 10,
	COM_STATE_READ_CRC = 11
} ComState;

#define BUFFERSIZE	64
typedef struct {
	/* Parameters and state*/
	uint8_t tx_buffer[BUFFERSIZE];
	uint8_t rx_buffer[BUFFERSIZE];
	ComState state;
	uint32_t auto_inc;
	uint32_t addr;
	MyVariable* var;
	uint32_t data_length;
	uint32_t buffer_index;
	uint32_t variable_index;
	uint8_t lrc_rec;
	uint8_t lrc_send;
} ComHandler;

static volatile ComHandler hcom;
uint8_t received_lrc = 0;


static inline void calculate_lrc_receive(uint8_t rec_byte) {
	hcom.lrc_rec ^= rec_byte;
}

static inline void calculate_lrc_send(uint8_t rec_byte) {
	hcom.lrc_send ^= rec_byte;
}

static inline uint8_t prepareVarsForTransmission(uint32_t currentInd) {
	hcom.var = MyVar_get_by_index(hcom.addr++);
	if (hcom.var != NULL) {
		//variable exists
		//copy content of the variable
		uint8_t *ptr = (uint8_t *) hcom.var->ptr;
		for (uint32_t i = 0; i < hcom.var->size; i++) {
			hcom.tx_buffer[currentInd + i] = ptr[i];
		}
		return COM_RESP_OK;
	} else {
		//variable does not exist, error state, send all FF
		return COM_RESP_ERR;
	}

}

/* communication functions */
uint8_t communication_step(uint8_t data) {
	static uint8_t response = 0x00;

	switch (hcom.state) {
	case COM_STATE_IDLE:
		if (data == 0x16) {
			led_red_on();
			hcom.state = COM_STATE_SYNC;
			calculate_lrc_receive(data);
			response  = (uint8_t) hcom.state;
		} else {
			response  = (uint8_t) 0xFF;
			return (uint8_t) 0xFF;
		}
		//hcom.state = COM_STATE_SYNC;
		//response  = data;
		break;
	case COM_STATE_SYNC:
		//response  = (uint8_t)hcom.state;
		hcom.data_length = data; //payload length
		hcom.state = COM_STATE_LEN;
		response  = (uint8_t) hcom.state;
		calculate_lrc_receive(data);
		break;
	case COM_STATE_LEN:
		hcom.addr = data;
		hcom.state = COM_STATE_REG_ADDR;
		response  = (uint8_t) hcom.state;
		calculate_lrc_receive(data);
		break;
	case COM_STATE_REG_ADDR:
		hcom.auto_inc = (data & COM_AUTOINC_MASK) ? 1 : 0;

		if (data & COM_WRITE) {
			//the command is writing the value to the STM
			hcom.state = COM_STATE_WRITE;
			response  = (uint8_t) hcom.state;
		} else if (data & COM_READ) {
			//the command is reading the value in the register from the STM
			hcom.state = COM_STATE_READ_RESP;
			response = prepareVarsForTransmission(0); //byte_ptr should be 0
			if (response == COM_RESP_ERR) {
				hcom.data_length = 0;
			}
			calculate_lrc_send(response);
		} else {
			response = COM_RESP_ERR;
			//communication_reset();
		}
		calculate_lrc_receive(data);
		break;
	case COM_STATE_WRITE:
		hcom.rx_buffer[hcom.buffer_index] = data;
		hcom.buffer_index++;
		response  = (uint8_t) hcom.state;
		if (hcom.buffer_index >= hcom.data_length) {
			hcom.state = COM_STATE_WRITE_CRC;
		}
		calculate_lrc_receive(data);
		break;
	case COM_STATE_WRITE_CRC:
		//response  = 0x0C;
		received_lrc = data;
		if (hcom.lrc_rec == data) {
			response = COM_RESP_OK;
		} else {
			//error occured, lrc do not match
			response = COM_RESP_ERR;
		}
		hcom.state = COM_STATE_WRITE_RESP;
		//prepare for the next transmission
		break;
	case COM_STATE_WRITE_RESP:
		response  = (uint8_t) 0x00;
		if (received_lrc == hcom.lrc_rec) {
			//lrc match each other
			hcom.var = MyVar_get_by_index(hcom.addr);
			if (hcom.var != NULL) {
				//store the first variable
				MyVar_set_value(hcom.var, hcom.rx_buffer);

				if (hcom.auto_inc) {
					//autoincrement is used
					uint32_t index = hcom.var->size;
					while (index < hcom.data_length) {
						//until all data is processed
						hcom.var = MyVar_get_by_index(++hcom.addr);
						if (hcom.var != NULL) {
							//variable exists, we can store it
							int *ptr = (int *) (hcom.rx_buffer + index);
							MyVar_set_value(hcom.var, ptr);
							index += hcom.var->size;
						} else {
							//variable does not exist
							index = hcom.data_length;
						}
					}
				}
			} else {
				//variable does not exist
			}
		}
		led_red_off();
		hcom.state = COM_STATE_IDLE;
		break;
	case COM_STATE_READ_RESP:
		//prepare for the next transmission
		if (hcom.data_length > 0) {
			hcom.state = COM_STATE_READ_DATA;
			response  = (uint8_t) hcom.tx_buffer[hcom.buffer_index];
			calculate_lrc_send((uint8_t) hcom.tx_buffer[hcom.buffer_index]);
			hcom.buffer_index++;
			hcom.variable_index++;
		} else {
			hcom.state = COM_STATE_READ_CRC;
			response  = (uint8_t) hcom.lrc_send;
		}
		break;
	case COM_STATE_READ_DATA:
		if (hcom.buffer_index >= hcom.data_length) {
			//all data was transmitted
			hcom.state = COM_STATE_READ_CRC;
			response  = (uint8_t) hcom.lrc_send;
		} else {
			//there are bytes to be transmitted
			response  = (uint8_t) hcom.tx_buffer[hcom.buffer_index];
			calculate_lrc_send((uint8_t) hcom.tx_buffer[hcom.buffer_index]);
			hcom.buffer_index++;
			hcom.variable_index++;
			if (hcom.variable_index == hcom.var->size) {
				hcom.variable_index = 0;
				prepareVarsForTransmission(hcom.buffer_index);
			}
		}
		break;
	case COM_STATE_READ_CRC:
		//
		response  = (uint8_t) 0x00;
		led_red_off();
		hcom.state = COM_STATE_IDLE;

		break;
	default: {
		//problem occured
		hcom.state = COM_STATE_IDLE;
		break;
	}


	}

	return response;
}

void communication_init(void) {
	hcom.state = COM_STATE_IDLE;
	hcom.var = NULL;
	hcom.data_length = 0;
	hcom.auto_inc = DISABLE;
	hcom.addr = 0;
	hcom.lrc_rec = 0;
	hcom.lrc_send = 0;
	hcom.buffer_index = 0;
	hcom.variable_index = 0;
}


