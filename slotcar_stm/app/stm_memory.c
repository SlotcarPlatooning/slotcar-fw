#include "stm_memory.h"
#include "myvariable.h"

Output stm_out;
Input stm_in;

void memory_init(){
	/*
	 * Outputs
	 */
	MyVar_register(0,stm_out.speed);
	MyVar_register(1,stm_out.distance_front);
	MyVar_register(2,stm_out.distance_back);
	MyVar_register(3,stm_out.acc_x);
	MyVar_register(4,stm_out.acc_y);
	MyVar_register(5,stm_out.acc_z);
	MyVar_register(6,stm_out.gyro_pitch);
	MyVar_register(7,stm_out.gyro_roll);
	MyVar_register(8,stm_out.gyro_yaw);
	MyVar_register(9,stm_out.motor_i);
	MyVar_register(10,stm_out.motor_bemf);
	MyVar_register(11,stm_out.v_rail);
	MyVar_register(12,stm_out.v_sc);
	MyVar_register(13,stm_out.distance_front_raw);
	MyVar_register(14,stm_out.distance_back_raw);
	MyVar_register(15, stm_out.speed_bemf);
	MyVar_register(16, stm_out.speed_irc);
	MyVar_register(17, stm_out.speed_kalman);
	MyVar_register(18, stm_out.friction);
	MyVar_register(19, stm_out.applied_pwm); //currently applied pwm for motor - output of any controller
	MyVar_register(20, stm_out.ref_i);
	MyVar_register(21, stm_in.ref_speed);

	/*
	 * Inputs
	 */
	MyVar_register(30,stm_in.ref_pwm); //reference pwm for the direct pwm setting at the motor
	MyVar_register(31,stm_in.ref_speed); //reference speed for the speed controller
	MyVar_register(32,stm_in.ref_distance); //reference distance for the distance controller
	MyVar_register(33,stm_in.reg); //regulator type
	MyVar_register(34,stm_in.dist_back_transmit_en);
	MyVar_register(35,stm_out.whois);
}




