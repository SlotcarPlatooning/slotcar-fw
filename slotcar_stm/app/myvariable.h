/*
 * Creates a index memory of variables spread among all memory, stores only pointers and size of the variable.
 */
#ifndef MYVARIABLE_H_
#define MYVARIABLE_H_

#include "stdint.h"
#include "stdlib.h"

/*
 * The size of the memory.
 */
#define MY_VARIABLES_MAX_COUNT 64

/*
 * Element of the memory.
 */
typedef struct{
    void* ptr; //pointer to a variable to the memory
    int size;
} MyVariable;

/*
 * @see MyVar_registeration
 * Wrapper, resolve pointer and size.
 * @param	addr	Index in the memory.
 * @param	var		Variable itself, NOT its pointer.
 */
#define MyVar_register(addr,var) MyVar_registeration(addr,(typeof(var)*)(&var),sizeof(var))

/*
 * @return pointer to a MyVariable by index.
 */
volatile MyVariable* MyVar_get_by_index(uint8_t index);

/*
 * Sets the value of a MyVariable passes by pointers.
 * @warning Memory copy, make sure you pass correct type.
 * @return  0 if success
 * @return -1 if failed
 */
int MyVar_set_value(MyVariable* var, void* value);

/*
 * @see MyVar_set_value, MyVariable is selected by index.
 */
int MyVar_set_value_by_index(uint8_t index, void* value);

/*
 * Registers a variable into the memory.
 * @return  0 if success
 * @return -1 if failed
 */
int MyVar_registeration(uint8_t address, volatile void* ptr, uint8_t size);

#endif /* MYVARIABLE_H_ */
