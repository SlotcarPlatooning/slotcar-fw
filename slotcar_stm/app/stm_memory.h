#ifndef STM_MEMORY_H_
#define STM_MEMORY_H_

typedef volatile struct {
	float ref_pwm;				// Reference PWM for regulation <0.0f,1.0f>
	float ref_speed;			// Reference speed for regulation [m/s]
	float ref_distance;			// @deprecated, Reference distance for regulation [m/s],
	int reg;					// @see RegType control.h
	int dist_back_transmit_en; 	// enables transmitting signal from distance sensor, 0 is disabled, ~0 is enabled
	int speed_meas_type;		// @see SpeedOutput
} Input;

typedef volatile struct {
	float speed;
	float distance_front;
	float distance_back;
	float acc_x;
	float acc_y;
	float acc_z;
	float gyro_pitch;
	float gyro_roll;
	float gyro_yaw;
	float mag_x;
	float mag_y;
	float mag_z;
	float motor_i;
	float motor_bemf;
	float i;
	float v_rail;
	float v_sc;
	float distance_front_raw;
	float distance_back_raw;
	float speed_bemf;
	float speed_irc;
	float speed_kalman;
	float friction;
	float applied_pwm;
	float ref_i;
	float motor_i2;
	int whois;
} Output;

/*
 * @brief Read-only measurements accessible via SPI
 */
extern Output stm_out;

/*
 * @brief Read-write settings variables accessible via SPI
 */
extern Input stm_in;

/*
 * @brief Makes @see stm_in and @see stm_out accessible via SPI @see communication.h
 */
void memory_init(void);
#endif /* STM_MEMORY_H_ */
