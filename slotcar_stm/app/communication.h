#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include <stdint.h>

uint8_t communication_step(uint8_t data);
void communication_init(void);

#endif /* COMMUNICATION_H_ */
