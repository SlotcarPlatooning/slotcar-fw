#include "myvariable.h"

/*
 * Stores registered MyVariables.
 */
volatile MyVariable list[MY_VARIABLES_MAX_COUNT];

int MyVar_registeration(uint8_t address, volatile void* ptr, uint8_t size) {
	if (address < MY_VARIABLES_MAX_COUNT) {
		volatile MyVariable *var = &list[address];
		var->ptr = ptr;
		var->size = size;
		return 0;
	} else {
		return -1;
	}
}

static inline volatile MyVariable* get_by_index(int index) {
	if (index >= 0 && index < MY_VARIABLES_MAX_COUNT) {
		return &list[index];
	} else {
		return NULL;
	}
}

static inline int set_value(MyVariable* var, void* value) {
	if (var != NULL || value != NULL) {
		memcpy(var->ptr, value, var->size);
		return 0;
	} else {
		return -1;
	}
}

volatile MyVariable* MyVar_get_by_index(uint8_t index) {
	return get_by_index(index);
}

int MyVar_set_value(MyVariable* var, void* value) {
	return set_value(var, value);
}

int MyVar_set_value_by_index(uint8_t index, void* value) {
	return set_value(get_by_index(index), value);
}
